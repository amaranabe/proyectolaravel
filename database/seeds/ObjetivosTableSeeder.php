<?php

use Illuminate\Database\Seeder;
use App\Objetivo;

class ObjetivosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insertamos datos
        Objetivo::create([
            'curso_id' => 1,
            'objetivo' => 'Desarrollar competencias esenciales para crear y llevar a cabo planes de apoyo personalizados.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 1,
            'objetivo' => 'Entender y aprender que debe existir un equilibrio entre lo que es importante para la persona y lo que es importante para apoyar a la persona.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 1,
            'objetivo' => 'Aumentar la voz de la persona, su capacidad de elección y de control.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 1,
            'objetivo' => 'Descubrir cómo se comunica la persona.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 1,
            'objetivo' => 'Aplicar las herramientas de planificación centrada en la persona para el desarrollo de equipos de profesionales centrados en la persona.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 3,
            'objetivo' => 'Enseñar al personal de atención directa 16 habilidades consideradas como esenciales para trabajar en la mejora conductual de personas con discapacidad intelectual.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 4,
            'objetivo' => 'Enseñar al personal de atención directa 27 habilidades, 11 de ellas consideradas críticas y necesarias para dirigir de forma efectiva al personal de atención directa.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 5,
            'objetivo' => 'Proporcionar información actualizada y entrenamiento en el desarrollo de habilidades y conocimientos que ayuden al personal a identificar las necesidades de salud mental de las personas con discapacidad intelectual.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 6,
            'objetivo' => 'Enseñar pautas para reconocer el inicio de la demencia.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 6,
            'objetivo' => 'Proporcionar conocimiento para desarrollar intervenciones apropiadas y a tener estrategias suficientes para afrontar esta enfermedad, incluyendo diferentes áreas: de salud, toma de medicación, actividades relevantes a lo largo del día,… .',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        Objetivo::create([
            'curso_id' => 7,
            'objetivo' => 'Formar en habilidades esenciales de supervisión y motivación al personal con el fin de mejorar la satisfacción en el trabajo y la calidad de los servicios, y el apoyo a las personas con discapacidad intelectual y del desarrollo.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
    }
}
