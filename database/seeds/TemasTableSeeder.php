<?php

use Illuminate\Database\Seeder;
use App\Tema;

class TemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        //insertamos datos
        Tema::create([
            'posicion' => 1,
            'titulo' => 'Qué es la discapacidad intelectual',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5,
        ]);

        Tema::create([
            'posicion' => 2,
            'titulo' => 'Qué es la demencia',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 3,
            'titulo' => 'Diagnóstico',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 4,
            'titulo' => 'Trabajar con diferentes realidades',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 5,
            'titulo' => 'Mantener una buena comunicación',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 6,
            'titulo' => 'Intervenciones terapéuticas',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 7,
            'titulo' => 'Problemas de conducta',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 8,
            'titulo' => 'Respuesta a las necesidades de dolor de las personas con demencia',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5,
        ]);

        Tema::create([
            'posicion' => 9,
            'titulo' => 'Experiencias y necesidades de los compañeros',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

        Tema::create([
            'posicion' => 10,
            'titulo' => 'Apoyo a prestar a la personas para que coman bien',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
            'curso_id' => 5
        ]);

    }
}

