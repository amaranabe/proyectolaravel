<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriasTableSeeder::class);
        factory(\App\User::class, 5)->create();            
        $this->call(UsersTableSeeder::class);
        $this->call(CursosTableSeeder::class);
        $this->call(ObjetivosTableSeeder::class);
        
        factory(\App\Tema::class, 1)->create();
        $this->call(TemasTableSeeder::class);

    }
}
