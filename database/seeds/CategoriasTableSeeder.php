<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	//Antes de ejecutar, borramos los registros existentes
    	DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
    	DB::table('categorias')->truncate(); //Vacíamos la tabla

        //Creamos un array para insertar en la tabla Cursos
        DB::table('categorias')->insert([
        	[
        		'nombre' => 'Discapacidad intelectual',
        		'descripcion' => null

        	],
        	[
        		'nombre' => 'Familias',
        		'descripcion' => null

        	],
        	[
        		'nombre' => 'Equipos y liderazgo',
        		'descripcion' => null

        	]
        ]);
    }
}