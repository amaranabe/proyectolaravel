<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	//Antes de ejecutar, borramos los registros existentes
    	DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
    	DB::table('cursos')->truncate(); //Vacíamos la tabla

    	//Creamos un array para insertar en la tabla Cursos
        DB::table('cursos')->insert([
        	[
        		'titulo' => 'Habilidades de pensamiento',
                'titulo2' => 'Planificación y acción centradas en la persona',
        		'slug' => 'habilidades-de-pensamiento',
        		'dirigido_a' => 'Toda persona implicada directamente o indirectamente en el apoyo a personas con discapacidad intelectual',
                'duracion' => '17',
        		'precio' => '50',
                'descuento' => '50',
        		'imagen' => 'images/curso1.jpg',
                'estado' => 2,
                'categoria_id' => 1,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Apoyo a padres y familiares',
        		'titulo2' => 'Guía de estrategias conductuales positivas',
                'slug' => 'apoyo-a-padres-y-familiares',
        		'dirigido_a' => 'Padres, cuidadores u otros familiares de personas con discapacidad intelectual y problemas de conducta.',
                'duracion' => '8',
        		'precio' => '50',
                'descuento' => '10',
        		'imagen' => 'images/curso2.jpg',
                'estado' => 2,
                'categoria_id' => 2,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Apoyo conductual positivo para personal de apoyo directo',
                'titulo2' => null,
        		'slug' => 'apoyo-conductual-positivo-para-personal-de-apoyo-directo',
        		'dirigido_a' => 'Al personal que apoya a personas con discapacidades intelectuales en diferentes servicios residenciales, ocupacionales, escolares u otros',
                'duracion' => '17',
        		'precio' => '30',
                'descuento' => '0',
        		'imagen' => 'images/curso3.jpg',
                'estado' => 2,
                'categoria_id' => 3,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Apoyo conductual positivo para personal supervisor',
                'titulo2' => null,
        		'slug' => 'apoyo-conductual-positivo-para-personal-supervisor',
        		'dirigido_a' => 'Al personal supervisor de servicios residenciales, ocupacionales, escolares u otros para personas con discapacidad intelectual',
                'duracion' => '25',
        		'precio' => '40',
                'descuento' => '0',
        		'imagen' => 'images/curso4.jpg',
                'estado' => 2,
                'categoria_id' => 3,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Salud mental y discapacidad intelectual',
                'titulo2' => null,
        		'slug' => 'salud-mental-y-discapacidad-intelectual',
        		'dirigido_a' => 'Al personal en su labor de apoyo a personas con discapacidad en una variedad de situaciones: en la comunidad, en servicios hospitalarios o en servicios especializados',
                'duracion' => '35',
        		'precio' => '40',
                'descuento' => '0',
        		'imagen' => 'images/curso5.jpg',
                'estado' => 1,
                'categoria_id' => 4,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Demencia y discapacidad intelectual',
                'titulo2' => null,
        		'slug' => 'demencia-y-discapacidad-intelectual',
        		'dirigido_a' => 'Todas aquellas personas y profesionales implicados en el apoyo de la persona con discapacidad intelectual y demencia (médicos, trabajadoras sociales, cuidadores y el personal de atención directa)',
                'duracion' => '18',
        		'precio' => '40',
                'descuento' => '0',
        		'imagen' => 'images/curso6.jpg',
                'estado' => 1,
                'categoria_id' => 1,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	],
        	[
        		'titulo' => 'Prácticas en liderazgo',
                'titulo2' => 'Programa para el desarrollo del personal de dirección de primera línea',
        		'slug' => 'practicas-en-liderazgo-para-directores-de primera-línea',
        		'dirigido_a' => 'Al personal que apoya a personas con discapacidades intelectuales en diferentes servicios residenciales, ocupacionales, escolares u otros',
                'duracion' => '3.5',
        		'precio' => '40',
                'descuento' => '0',
        		'imagen' => 'images/curso7.jpg',
                'estado' => 2,
                'categoria_id' => 3,
        		'created_at' => new DateTime,
        		'updated_at' => new DateTime
        	]
        ]);

    }
}