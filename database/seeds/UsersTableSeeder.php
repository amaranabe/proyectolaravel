<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear usuarios
        User::create([
        	'name'=>'Teinei',
        	'email'=>'prueba@gmail.com',
        	'password'=>bcrypt('123456'),
            'confirmado'=> 1, 
            'remember_token' => str_random(10)
        ]);

        User::create([
        	'name'=>'Admin',
        	'email'=>'admin@gmail.com',
        	'password'=>bcrypt('123456'),
        	'admin'=> true, 
            'confirmado'=> 1,
            'remember_token' => str_random(10)
        ]);
    }
}
