<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     * Función para agregar nuevas tablas, columnas o índices a la BS
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 255);
            $table->string('titulo2')->nullable();
            $table->string('slug');
            $table->text('dirigido_a');
            $table->decimal('duracion');
            $table->decimal('precio', 5, 2);
            $table->integer('descuento')->nullable();
            $table->string('imagen', 300)->nullable();
            $table->enum('estado', [
                \App\Curso::PENDIENTE,
                \App\Curso::PUBLICADO,
                \App\Curso::ELIMINADO
            ])->default(\App\Curso::PUBLICADO);
            $table->timestamps();
            $table->softDeletes()->nullable;
        });
    }

    /**
     * Reverse the migrations.
     * Función para invertir las operaciones realizadas por el método up
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
