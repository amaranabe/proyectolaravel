<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleCestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_cestas', function (Blueprint $table) {
            $table->increments('id');

            //FK cesta_id
            $table->integer('cesta_id')->unsigned();
            $table->foreign('cesta_id')->references('id')->on('cestas')->onDelete('cascade');

            //FK curso_id
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');
            $table->integer('precio'); // % int
            $table->integer('descuento')->default(0); // % int
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_cestas');
    }
}
