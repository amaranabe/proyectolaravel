<?php

use Faker\Generator as Faker;

$factory->define(App\Tema::class, function (Faker $faker) {
	static $number = 1;
    return [   	
        'titulo' => $faker -> sentence,
        'descripcion' => $faker ->text,
        'posicion' => $number++,
        'curso_id' => App\Curso::all()-> random() -> id
    ];
});


