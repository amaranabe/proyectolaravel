<?php

use Faker\Generator as Faker;

$factory->define(App\Rating::class, function (Faker $faker) {
    return [
        'curso_id' => \App\Curso::all()->random() -> id,
        'user_id' => \App\User::all()->random() -> id,
        'rating'=> $faker-> randomFloat(2, 1, 5)
    ];
});
