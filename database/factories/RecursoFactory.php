<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'curso_id' => App\Curso::all()-> random() -> id,
        'objetivo'=> $faker->sentence
    ];
});
