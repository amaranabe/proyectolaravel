<?php

use Faker\Generator as Faker;

$factory->define(App\Objetivo::class, function (Faker $faker) {
    return [
        'curso_id' => App\Curso::all()-> random() -> id,
        'objetivo'=> $faker->sentence
    ];
});
