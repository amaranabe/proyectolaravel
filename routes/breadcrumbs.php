<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('admin'));
});

// Home > Cursos
Breadcrumbs::register('cursos', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cursos', route('cursos'));
});

// Home > Usuarios
Breadcrumbs::register('usuarios', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Usuarios', route('usuarios'));
});

// Home > Compras
Breadcrumbs::register('compras', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Compras', route('compras'));
});


// Home > Cursos > crear
Breadcrumbs::register('crear', function ($breadcrumbs) {	    	
    $breadcrumbs->parent('cursos');
    $breadcrumbs->push('crear', route('cursos.crear'));

});


// Home > Cursos > Titulo
Breadcrumbs::register('ver', function ($breadcrumbs, $curso) {	    	
    $breadcrumbs->parent('cursos');
    $breadcrumbs->push($curso->titulo, route('curso', $curso));

});

// Home > Cursos > Titulo > editar
Breadcrumbs::register('editar', function ($breadcrumbs, $curso) {
    $breadcrumbs->parent('ver', $curso);
    $breadcrumbs->push('editar', route('cursos.editar', $curso));

});





