<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| ----- Definimos las rutas que va a tener nuestra aplicación -------------
|
| ** Para que las rutas funcionen hemos de activar el módulo rewrite de apache, 
| y cambiar la directiva AllowOverride de apache2.conf **
|
|
*/
 

/** 
 * Inicio ------------------
 */
Route::get('/', "CursoController@index"); // cargar oferta de cursos al inicio

Route::get('/busquedas', "CursoController@busqueda"); 
Route::get('/cursos/json', "CursoController@data");

Route::get('/cursos', "CursoController@catalogo");
Route::post('/contacto', "EmailsController@postContact");

Route::get('/curso/{slug}', "CursoController@show")->name('detalle-curso'); // ver detalle del curso

Route::get('/cursos/elearning/{fileName}', function() {
	dd('hola');
})->where(['fileName' => '.*']);


/** 
 * Autenticación usuario------------
 */
Route::group(['middleware' => 'prevent-back-history'],function(){

	Auth::routes();
	Route::get('/home', 'CursoController@index')->name('home');
	Route::get('users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation'); // Verificación Email
	Route::get('/login/{driver}', 'Auth\LoginController@redirectToProvider')->name('social_auth');
	Route::get('/login/{driver}/callback', 'Auth\LoginController@handleProviderCallback');


	/** Usuario ----------- */
	Route::middleware('auth')->group(function() {
		Route::get('/misdatos/editar',  'UserController@edit')->name('verperfil');
		Route::post('/user/editar-perfil',  'UserController@update')->name('editarperfil');
		Route::post('user/editar-cuenta', 'UserController@updatePassword')->name('changepassword');
		Route::delete('/user/baja',  'UserController@destroy')->name('baja');

		/** Panel de alumno ----------- */
		Route::get('/miscursos', "AlumnoController@index")->name('miscursos');
		Route::post('/add_valoracion', 'CursoController@addRating');
		Route::get('/curso/{slug}/learning', "AlumnoController@show")->name('cursar');
		Route::get('/curso/learning/show/{id}', 'AlumnoController@showFile')->name('curso.recurso.showfile');

	});


});

/** 
 * Cesta ---------------------
 */
Route::post('/cesta', 'DetalleCestaController@store'); // add item
Route::get('/cesta', 'CestaController@index')->name('cesta'); //show cesta
Route::delete('/cesta', 'DetalleCestaController@destroy'); // delete item
Route::get('/cesta/trash', 'CestaController@trash')->name('cesta-trash'); //vaciar cesta
Route::get('/cesta/checkout', 'CestaController@checkout') ->middleware('prevent-back-history','auth'); //inicio de pago
Route::post('/cesta/pago', 'CestaController@payment'); // confirmación pago


/** 
 * Panel de Administración ---
 */

Route::middleware(['prevent-back-history', 'auth', 'admin'])->prefix('admin')->namespace('Admin')->group(function () {

    Route::get('/', "CursoController@index")->name('admin'); //index admin
	Route::get('/usuarios', 'CursoController@viewInscritos')->name('usuarios');
	Route::get('/inscritos', 'CursoController@getUsers')->name('inscritos');
	Route::get('/compras', 'CursoController@getCursosComprados')->name('compras');

	// Cursos
    Route::group(['prefix' => 'cursos'], function() {

	    Route::get('/', "CursoController@getAllCursos")->name('cursos'); //listado
	    Route::get('/crear', "CursoController@create")->name('cursos.crear'); // formulario crear
	    Route::get('/{curso}', "CursoController@show")->name('curso'); //listado
		Route::post('/', "CursoController@store")->name('cursos.store'); // registrar form crear
		Route::get('/{slug}/editar', "CursoController@edit")->name('cursos.editar'); // formulario editar
		Route::put('/{curso}', "CursoController@update")->name('cursos.update'); // registrar 
		Route::delete('/{id}', "CursoController@destroy"); // delete
		Route::put('/{slug}/publicar', "CursoController@publicar")->name('cursos.publicar'); // publicar

		// Temas
		Route::resource('/temas', 'TemaController')->except([
		    'index', 'create', 'show', 'edit'
		]);
		Route::post('/temas/update-posicion', 'TemaController@updatePosicion')->name('temas.posicion');

		// Recursos
		Route::get('/temas/recursos/show', 'RecursoController@getAllRecursos')->name('recursos.getRecursos');
		Route::get('/temas/recursos/{id}', 'RecursoController@getRecurso');
		Route::post('/temas/recursos', 'RecursoController@store')->name('recursos.store');
		Route::put('/temas/recursos/{id}', 'RecursoController@update')->name('recursos.update');
		Route::delete('/temas/recursos/destroy', 'RecursoController@destroy')->name('recursos.destroy');
		Route::get('/temas/recurso/{id}', 'RecursoController@getFile')->name('recursos.display');


	});



});



/** DE PRUEBA --------------------- */
Route::get('admin/cursos/hola', function() {

	  //echo phpinfo();

});