<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetalleCesta;

/**
 * Clase que va a contener las cestas de compra de cada usuario (su estado 
 * será activo) 
 */
class Cesta extends Model
{

    /**
     * Método que crea relación uno a muchos, entre los modelos Cesta y DetalleCesta
     * 
     * @return [DetalleCesta] [detalles asociadas a la cesta]
     */
    public function detalles()
    {
    	//dado que una cesta va a tener muchos detalles definimos una función
    	return $this ->hasMany(DetalleCesta::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

}
