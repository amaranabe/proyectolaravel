<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

use App\Objetivo;

class Curso extends Model
{
    
    use SoftDeletes;

    //creamos unas constantes para identificar el estado del curso
	const PENDIENTE = 1;
	const PUBLICADO = 2;
	const ELIMINADO = 3;

    protected $fillable = ['titulo', 'titulo2', 'slug', 'dirigido_a', 'duracion', 'precio', 'descuento', 'imagen', 'estado', 'categoria_id'];

    protected $withCount = ['ratings', 'users', 'objetivos']; //definimos esta variable para disponer de esos conteos en las consultas.


    // Creamos este método para que se puedan guardar también los objetivos, si existen se actualizarán y sino se crearán
    public static function boot() {
        parent::boot();

        //definimos evento tanto cuando se inserta como cuando se actualiza
        static::saved(function (Curso $curso) {
            //comprobar que no se está ejecutando la consola para evitar que al ejecutar seed, también se ejecute este guardado
            if(! \App::runningInConsole()) {

                //comprobar que existen objetivos
                if(request('objetivos')) {
                    foreach (request('objetivos') as $key => $objetivoFromInput) {
                        # code...
                        if($objetivoFromInput) {
                            Objetivo::updateOrCreate(['id' => request('objetivo_id'.$key)], [
                                'curso_id' => $curso->id,
                                'objetivo' => $objetivoFromInput
                            ]);
                        }
                    }
                }
            }
        });
    }
	
    //Importante crear relaciones entre Modelos. Es decir, aunque en la base de datos, si estén relacionados, en el modelo no, por eso utilizaremos ORM...
    
    //creamos accessor
    public function getUrlAttribute() {
    	return '/images' . $this->imagen;
    }

    public function getRouteKeyName() {
        return 'slug'; //permite buscar también por slug
    }

    /* Relaciones ORM con ELOQUENT --------------- */
    // Relación uno a muchos inversa -> Un curso pertenece a una categoría
    public function categoria() {
    	return $this->belongsTo(Categoria::class)->select('id', 'nombre');
    }

    //Relación uno a muchos ->Un curso va a tener uno o más objetivos
    // Si seleccionamos como dato un id, es necesario seleccionar todas las FK también
    public function objetivos() {
    	return $this->hasMany(Objetivo::class)->select('id', 'curso_id', 'objetivo');
    }

    //Relación uno a muchos ->Un curso va a tener una o más valoraciones
    public function ratings() {
    	return $this->hasMany(Rating::class);
    }

    //Relación uno a muchos ->Un curso va a tener uno o más requisitos
    public function requisitos() {
    	return $this->hasMany(Requisito::class)->select('id', 'curso_id', 'requisito');
    }

    //Relación muchos a muchos ->Un curso puede tener uno o más usuarios inscritos
    public function users() {
    	return $this->belongsToMany(User::class);
    }

    public function temas() {
        return $this->hasMany(Tema::class);
    }


    public function detallecestas() {
        return $this->hasMany(Curso::class);
    }


    public function getValoracionAttribute() {
        return $this->ratings->avg('valoracion');
    }

    public function cursosRelacionados() {
        return Curso::with('ratings')
                ->whereCategoriaId($this->categoria->id)
                ->where('id', '!=', $this->id)
                ->latest()
                ->get();
    }


    //Creamos un Scope: permite utilizar las consultas que se van a hacer de forma repetida definirlas como funciones para evitar que se dupliquen
    public function scopeBusqueda($query, $titulo) {
            
        return $query ->where('titulo', 'like', "%$titulo%");
    }

    public function scopeTotalInscritos() {
        return DB::table('curso_user')->count();
    }


    public function getTotal() {
        $precio = $this->precio;
        return $this->precio-($precio*$this->descuento/100);

    }


}
 