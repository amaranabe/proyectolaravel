<?php

namespace App\Policies;

use App\User;
use App\Curso;
use App\DetalleCesta;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Collection;


/**
 * CursoPolicy para controlar qué usuario está inscrito al curso o 
 * registrado en la cesta. La utilizamos en la vista para controlar
 * productos a añadir a la cesta y no duplicar.
 * 
 */
class CursoPolicy
{
    use HandlesAuthorization;

    /**
     * Comprobar si un usuario se puede inscribir o no a un curso
     * 
     * @param  User   $user  
     * @param  Curso  $curso 
     * @return boolean 
     */
    public function inscribir(User $user, Curso $curso) {
        // false puede inscribirse
        return ! $curso->users->contains($user->id); 
    }

    /**
     * Comprobar si una usuario puede añadir un curso a la cesta
     * 
     * @param  User   $user  
     * @param  DetalleCesta  $curso 
     * @return boolean 
     */
    public function addCursoCesta(User $user, Curso $curso){
            $detallescesta = $user->cesta->detalles;
        
        // false puede añadirse a la cesta
        // contains(): we pass a key / value pair to the contains method, 
        // which will determine if the given pair exists in the collection
        return ! $detallescesta->contains('curso_id', $curso->id); 
    }


    /**
     * Determine whether the user can view the curso.
     *
     * @param  \App\User  $user
     * @param  \App\Curso  $curso
     * @return mixed
     */
    public function view(User $user, Curso $curso)
    {
        return $curso->users->contains($user->id);
    }


    /**
     * Determine whether the user can make rating to the curso.
     *
     * @param  \App\User  $user
     * @param  \App\Curso  $curso
     * @return mixed
     */
    public function valorar(User $user, Curso $curso)
    {
        return ! $curso->ratings->contains('user_id', $user->id);
    }


}
