<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Curso;
use DB;

class DetalleCesta extends Model
{
    
    protected $fillable = [
        'cesta_id', 'curso_id', 'precio', 'descuento'
    ];

    // N DetalleCesta        1 Curso -> un curso se puede asociar con varios detalles de la cesta, pero al revés la cesta tendrá un curso determinado.
    public function curso() {
    	return $this->belongsTo(Curso::class, 'curso_id');
    }

    public function cesta() {
    	return $this->belongsTo(Cesta::class, 'cesta_id');
    }

//COMPROBAR SU NO USO
    public static function isCurso($idCurso, $idUser) {
        // SQL
        // SELECT `detalle_cestas`.`id`, detalle_cestas.cesta_id, detalle_cestas.curso_id, cestas.user_id FROM `detalle_cestas`, `cestas` WHERE `detalle_cestas`.`cesta_id`=`cestas`.`id` AND `detalle_cestas`.`curso_id` = 1 AND cestas.user_id=6;

        $cursos=DB::table('detalle_cestas')
            ->join('cestas', 'cestas.id', '=', 'detalle_cestas.cesta_id')
            ->where('curso_id', $idCurso)->where('user_id', $idUser)
            ->get();

    	return $cursos;
    }

    public function getTotal() {
        $precio = $this->precio;
        return $this->precio-($precio*$this->descuento/100);

    }
    
}
