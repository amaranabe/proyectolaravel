<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    

	protected $fillable = ['posicion', 'titulo', 'descripcion', 'curso_id'];

    public function curso() {
    	return $this->belongsTo(Curso::class);
    }


    public function recursos() {
    	return $this->hasMany(Recurso::class);
    }
}
