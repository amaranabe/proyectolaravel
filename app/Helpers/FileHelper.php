<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
  
class FileHelper {

	//Función que guarda imagen en el disco local, directorio storage/uploads
	public static function uploadFile($key, $path) {
		
		// Guardar imagen en el directorio  storage/uploads/ + el path
		$pathUploadImageInStorage = request()->file($key)->store($path);
		// Recogemos solo en nombre del archivo
		$filename = pathinfo($pathUploadImageInStorage, PATHINFO_FILENAME );
		//Recogemos solo la extensión
		$extension = pathinfo($pathUploadImageInStorage,  PATHINFO_EXTENSION);
		// Componemos el nombre de la ruta para almacenar
		$fileNameToStore = $path . "/" . $filename . '.' . $extension;
		return $fileNameToStore;
	}

	

	//Función que guarda imagen en el disco public que guarda en el directorio  storage/app/public/images
	public static function uploadImage($key, $path) {
		
		// Guardar imagen en el directorio  storage/app/public/images
		$pathUploadImageInStorage = request()->file($key)->store($path, 'public');
		// Recogemos solo en nombre del archivo
		$filename = pathinfo($pathUploadImageInStorage, PATHINFO_FILENAME );

		//Recogemos solo la extensión
		$extension = pathinfo($pathUploadImageInStorage,  PATHINFO_EXTENSION);
		// Componemos el nombre de la ruta para almacenar
		$fileNameToStore = $path . "/" . $filename . '.' . $extension;
		return $fileNameToStore;

	}

}

