<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Validator;
use Hash;
use App\Cesta;
use App\DetalleCesta;
use App\User;
use Illuminate\Support\Facades\Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Instrucción para evitar error con la base de datos de un tipo de dato determinado que tiene problemas con la version de mysql
        Schema::defaultStringLength(191); //Añadimos la clase Schema


        // Utilizamos el composer view para crear una variable conter () al que se pueda acceder desde todas las vistas.
        view()->composer('*',function($view) {
            if(auth()->user()) {
                $view->with('conter', count(auth()->user()->cesta->detalles));
            } else {
                //$view->with('conter', 0);
                if(session()->has('cesta_anonima')) {
                    $view->with('conter', count(session('cesta_anonima')));
                } else {
                    $view->with('conter', 0);
                }
            }

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
