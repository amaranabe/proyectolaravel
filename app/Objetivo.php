<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objetivo extends Model
{
    
	protected $fillable = ['curso_id', 'objetivo'];

    // Relación uno a muchos inversa
    public function curso() {
    	return $this->belongsTo(Curso::class);
    }
}
