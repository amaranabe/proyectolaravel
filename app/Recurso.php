<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    protected $fillable = ['tema_id', 'titulo', 'file', 'size', 'tipo', 'extension', 'mimetype'];

    public function tema() {
    	return $this->belongsTo(Tema::class);
    }

}
