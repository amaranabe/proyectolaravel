<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{

    
    /**
     * Handle an incoming request. => Permite manejar la solucitud de entrada
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //metodo auth y check permite conocer si usuario ha iniciado sesión
        //si no es administrador le redirigimos inicio
        if (!auth()->user()->admin) {
            abort(401, __("No tienes acceso a esta zona"));
                     
        }        
        return $next($request);  
    }
}
