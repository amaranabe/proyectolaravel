<?php

namespace App\Http\Middleware;

use Closure;

class PreventBackHistory
{
    /**
     * Handle an incoming request.
     * Inhabilita el historial de botones de retorno
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (method_exists($response, 'header')) {
            $response->header('Cache-Control','no-cache, no-store, max-age=0, must-revalidate');
            $response->header('Pragma','no-cache');
            $response->header('Expires','Sun, 02 Jan 1990 00:00:00 GMT');
        }
        
        return $response;

    }
}
