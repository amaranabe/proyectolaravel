<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\User;
use App\Rating;

class CursoController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $cursos = Curso::with('categoria', 'ratings')
            ->where('estado', Curso::PUBLICADO)->get();
        
        //Generamos una vista a la que pasamos el objeto Curso
        return view('index', compact('cursos'));
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        // Devuelve una colección vacía en caso de no encontrar registro
        $curso = Curso::with(
            [
                'categoria' => function($query) {
                    $query->select('id', 'nombre');
                }, 
                'objetivos' => function($query) {
                    $query->select('id', 'curso_id', 'objetivo');
                }, 
                'temas' => function($query) {
                    $query->select('id', 'curso_id', 'posicion', 'titulo', 'descripcion')->orderBy('posicion');
                }, 
                'ratings.user'
            ]
        )->withCount(['users', 'ratings'])
         ->where('slug', $slug)
         ->first();
        
        $from = false; //no inscrito
        $relacionados = $curso->cursosRelacionados();
        return view('curso', compact('curso', 'relacionados', 'from'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function catalogo()
    { 
        $cursos = Curso::withCount(['users'])
            ->with('categoria', 'ratings')
            ->where('estado', Curso::PUBLICADO)
            ->get();
        //dd($cursos);
        //Generamos una vista a la que pasamos el objeto Curso
        return view('catalogo', compact('cursos'));
    }


    public function busqueda(Request $request)
    { 
        $query = $request->consulta;
        $cursos = Curso::Busqueda($query)
            ->paginate(6);

        //Si solo hay un curso de busqueda que me envíe a él directamente
        if ($cursos -> count() == 1) {
            $slug= $cursos->first()->slug;
            return redirect('curso/' . $slug);
        }

        //Enviamos el objeto Curso a la vista
        return view('busquedas', compact('cursos', 'query'));
    }


    //Metodo que devuelve un array con todos los titulos del curso
    public function data()
    { 
        $cursos = Curso::pluck('titulo');
        return $cursos;
    }

    // Método que añade una valoración a un curso
    public function addRating () {
        $rating = new Rating();
        $rating->user_id = auth()->user()->id;
        $rating->curso_id = request('curso_id');
        $rating->valoracion = request('rating_input');
        $rating->comentario = request('msj_valoracion');
        $rating->save();

        return back()->with('mensaje', 'Muchas gracias por valorar el curso');

    }


}
