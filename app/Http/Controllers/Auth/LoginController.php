<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\DetalleCesta;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Get the post register / login redirect path users after login.
     * @override from trait RedirectsUsers 
     * @return string
     */
    public function redirectTo()
    {
        if(auth()->user()->admin) {
            return '/admin';
        }
        $redirect = redirect()->home();
        //dd($redirect);
        return '/home';
    }



    /**
     * Handle an authentication attempt.
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function authenticated(Request $request, $user)
    { 
        // Comprobar si usuario ha confirmado su cuenta
        if (!$user->confirmado) {
            auth()->logout();
            return back()->with('notificacion', 'Debes confirmar tu registro. Te hemos enviado un email de confirmación de registro. Por favor, revísalo.');
        }

        // Comprobamos la existencia de cesta anónima
        // Si existe, debemos descartar aquellos que ya existen
        // y volcar la cesta anónima con la cesta de la BD
        if(session()->has('cesta_anonima')) {

            $ca = session()->get('cesta_anonima'); //cesta anonima
            $cestas=auth()->user()->cestas; //todas las cestas del usuario
            //recorro cada elemento de la cesta anónima
            foreach ($ca as $key => $item) {
                $existeCurso=false;
                //busco cada elemento de la cesta anónima en todas las cestas del usuario
                foreach ($cestas as $cesta) {

                    //Compruebo si curso existe en la cesta                   
                    $existeCurso= $cesta->detalles->contains('curso_id', $item->curso_id);
                    if($existeCurso) {
                        break;
                    }             
                }
   
                if (!$existeCurso) {
                    $detalleCesta = new DetalleCesta();
                    //recogemos en la BBDD  el id cesta
                    $detalleCesta->cesta_id = auth()->user()->cesta-> id;
                    $detalleCesta->curso_id = $item-> curso_id;
                    $detalleCesta->precio = $item-> precio;
                    $detalleCesta->save();
                } 
            }

            // eliminar la sesión
            session()->forget('cesta_anonima');
        }

        return redirect()->intended($this->redirectPath()); //el método intended, redirige al usuario a la URL a la que intentaba acceder antes de ser interceptado por el middleware de autenticación
    }
}
