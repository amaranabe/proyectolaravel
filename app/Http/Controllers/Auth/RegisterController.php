<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\EmailUserConfirmation;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'apellido1' => 'nullable|alpha',
            'apellido2' => 'nullable|alpha',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {        
          return User::create([
            'name' => $data['name'],
            'apellido1' => $data['apellido1'],
            'apellido2' => $data['apellido2'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Handle a registration request for the application.
     * (trait RegistersUsers)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function register(Request $request) {

        $input = $request->all();
        $validator = $this->validator($input);

        if($validator->passes()) {
            $data = $this->create($input)->toArray();

            $data['confirmation_code'] = str_random(25);

            $user = User::find($data['id']);
            $user->confirmation_code = $data['confirmation_code'];
            $user->save();

            //Enviar mail de confirmación (confirmation_code)
            // Para que pueda acceder a la variable $data, hacemos uso de USE
            /*Mail::send('emails.mensaje_confirmacion', $data, function ($message) use ($data) {    
                $message->to($data['email'], $data['name'])->subject('Por favor, confirma tu correo.');
            });*/
            Mail::to($user)->send(new EmailUserConfirmation($user));
            return redirect(route('login'))->with('notificacion', '¡Gracias por registrarte! El email de confirmación ha sido enviado. Por favor, revise su correo.');
        }

        return redirect(route('register'))->withErrors($validator)->withInput();
    }

    /**
     * Recoge el enlace de verificación enviado por mail
     * @return
     * 
     */
    public function confirmation($token)
    {
        $user = User::where('confirmation_code', $token)->first();

        if (!is_null($user)) {
            $user->confirmado = true;
            $user->confirmation_code = null;
            $user->save();
            return redirect(route('login'))->with('notificacion', '¡Bienvenido! El proceso de registro se ha realizado correctamente.');
        }  
        return redirect(route('register'))->with('notificacion', 'Lo sentimos. Algo ha ido mal en el proceso de registro.');
    }

}
