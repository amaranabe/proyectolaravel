<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\FromContactForm;
use Mail;

class EmailsController extends Controller
{
    //método que valida el formulario de contacto y envía un mail
    public function postContact(Request $request) {
        
    	//Validamos formulario de contacto
    	$this->validate($request, [
    		'nombre' => 'required',
    		'email' => 'required|email',
            'asunto' => 'required|min:3',
    		'mensaje' => 'required|min:10']);

        $data = array(
            'nombre' => $request->nombre,
            'email' => $request->email,
            'asunto' => $request->asunto,
            'cuerpoDelMensaje' => $request->mensaje );    
      
       //Para personalizar el correo y utilizar la librería Markdown hago uso de la clase Mailable FromContactForm creada
       //Llamo a la clase mail y le envío, la clase FromContactForm, un array con los datos y una función que ejecutará el envío.
       Mail::send(new FromContactForm($data), $data, function($mail) use($data) {
            $mail->from($data['email'], $data['nombre']);
        });

        $aviso_email='El email ha sido enviado correctamente. En breve recibirá respuesta.';
    	return redirect('/#contacto')->with(compact('aviso_email'));
    }
}
