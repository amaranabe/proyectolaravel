<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Recurso;


class AlumnoController extends Controller
{
    /**
     * Display a listing of the buying courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $miscursos = auth()->user()->cursos->all();
        //Generamos una vista a la que pasamos el objeto Curso
        return view('alumnos.miscursos', compact('miscursos'));
    }



    /**
     * Display a listing of the curso to learning.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $curso = Curso::where('slug', $slug)
                ->first();
        $from = true; // inscrito
        return view('alumnos.elearning', compact('curso', 'from'));
    }


    /** Función que devuelve un fichero en el Modulo de aprendizaje
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function showFile(Request $request) {

        $recurso=Recurso::find($request->id);
        $path=$recurso->file; // obtiene el path del fichero


        //guarda el fichero
        $pathPublic=storage_path("uploads/" . $path);

        //define posibles headers
        $type = "application/pdf";
        header('Content-Type:'.$type);
        header('Content-Length: ' . filesize($pathPublic));

        return response()->file($pathPublic);
    }


}
