<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Curso;
use App\Tema;

class TemaController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        // Validar
        $validator = Validator::make($request->all(), [
            'title' => 'required | min:10',
            'descripcion' => 'nullable | min:10'
        ]);

        if($validator->fails()) {
              return back()->withErrors($validator, 'add_tema')->withInput();
        }

        //última posicion
        $last_posicion=Tema::where('curso_id', $request->curso_id )->pluck('posicion')->last();

        // Asignar valores
        $tema = new Tema();
        $tema->posicion = ++ $last_posicion;
        $tema->titulo = $request->title;
        $tema->descripcion = $request->descripcion;
        $tema->curso_id = $request->curso_id;

        //Guardar
        $tema -> save();

        $slug = Curso::where('id', "=", $tema->curso_id)->first()->slug;
        //Redirigir a la vista tras obtener el slug a enviar
        session()->flash('alert-class', 'alert-info'); 
        return redirect()->route('curso', ['slug' => $slug])->with('aviso','El tema número ' . $tema->posicion . ' ha sido creado satisfactoriamente.');


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Validar
        $validator = Validator::make($request->all(), [
            'title' => 'required | min:10',
            'descripcion' => 'nullable | min:10'
        ]);

        if($validator->fails()) {
              return back()->withErrors($validator, 'edit_tema')->withInput();
        }

        // Asignar y guardar valores
        $tema = Tema::find($request->tema_id);
        $tema->titulo = $request->title;
        $tema->descripcion = $request->descripcion;
        $tema -> save();
        session()->flash('alert-class', 'alert-info'); 
        return back()->with('aviso','El tema número ' . $tema->posicion . ' ha sido modificado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tema=Tema::find($id);
        $posicion = $tema->posicion;
        $curso_id = $tema->curso_id;        
        $tema->DELETE(); // DELETE
        
        
        // Actualizar posicion de los temas
        // vamos a reposicionar todos los temas que pertenecen al curso del tema que queremos borrar
        // se obtendran ordenados y se asignaran en orden sus nuevas posiciones
        $listado_temas=Tema::where('curso_id', $curso_id)->orderBy('posicion', 'ASC')->get();
        
        foreach ($listado_temas as $i => $tema) {
            $tema->posicion=$i+1;
            $tema->save();
        }

        // llamar a la vista
        session()->flash('aviso', 'El tema ha sido eliminado correctamente.');
        session()->flash('alert-class', 'alert-danger'); 
        return back();
    }


    /**
     * Update posicion attribute from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePosicion(Request $request)
    {
        $temas = $request->temas;
        foreach ($temas as $key => $posicion) {
            //comprobar que no hay valores vacíos en el array
            if(!empty($key) && !empty($posicion)) {
                $tema = Tema::find($key);
                $tema->posicion = $posicion;
                $tema->save();
            }
        }
        return response()->json(['success' => true, 'message' => 'Success'], 200);

    }
}
