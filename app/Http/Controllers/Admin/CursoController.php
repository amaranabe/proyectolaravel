<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Curso;
use App\User;
use App\Cesta;
use App\DetalleCesta;
use DataTables;
use File;
use App\Http\Requests\CursoRequest;
use App\Helpers\FileHelper;
use Illuminate\Support\Facades\Storage;

class CursoController extends Controller
{   
        
    /**
     * Display a listing queries of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Curso::withCount('users')->orderBy('created_at', 'DESC')->paginate(6); //muestra cursos de 6 en 6
        $publicados = Curso::where('estado' , 2)->count(); // nº publicados
        $guardados = Curso::where('estado' , 1)->count(); // nº pendientes

        $registrados = User::where('id','!=', auth()->user())->count(); //nº usuarios registrados, excepto el usuario registrado
        $inscritosTotales = Curso::TotalInscritos();

        $ingresos = Cesta::join('detalle_cestas', 'detalle_cestas.cesta_id', 'cestas.id')->where('cestas.estado', 'finalizado')->sum('detalle_cestas.precio');

        //al devolver la vista, inyectamos todas las variables
        return view('admin.home')->with(compact('cursos', 'publicados', 'guardados', 'registrados', 'inscritosTotales', 'ingresos'));
    }


    /**
     * Display a listing of the resource cursos.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCursos()
    {
        $cursos = Curso::withCount('users')->orderBy('created_at', 'DESC')->paginate(10);
        return view('admin.cursos')->with(compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //creo una instancia de curso para guardar o editar
        $curso_ = new Curso();
        $btnTexto = __('Enviar curso');
        // ver formulario registro curso
        return view('admin.cursos.crear')->with(compact('curso_', ('btnTexto'))); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $cursoRequest)
    { 
        // El objeto $cursoRequest ya ha sido validado
        // Guardamos curso en la BD
        $imagen = FileHelper::uploadImage('image', 'images'); //guardamos imagen en storage/app/public/images
        $cursoRequest->merge(['slug' => str_slug($cursoRequest->titulo, '-')]);
        $cursoRequest->merge(['imagen' => $imagen]);
        $cursoRequest->merge(['estado' => Curso::PENDIENTE]);
        $curso_ = Curso::create($cursoRequest->input());


        //Creamos las variables que queremos enviar a la vista nueva
        $cursos = Curso::orderBy('id', 'desc')->paginate(3);
        $btnTexto = __('Crear Temas');

        //Creamos mensajes para enviar a la vista
        $cursoRequest->session()->flash('mensaje', 'Curso guardado correctamente. Recuerda que está en estado Pendiente.');

        //Redirigimos a la vista
        return view('admin.cursos.ver')->with(compact('cursos', 'curso_', ('btnTexto')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        $curso_=Curso::where('slug', $curso->slug)
            ->with([
                'temas' => function($query) {
                            $query->orderBy('posicion', 'ASC');
                        }
                ])
            ->firstOrFail();
            
            //dd($curso_);
        $cursos = Curso::paginate(5);
        return view('admin.cursos.ver')->with(compact('cursos', 'curso_')); //listado
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($param)
    {
        $curso_ = Curso::where('id', $param)
            ->orWhere('slug', $param)
            ->firstOrFail();
        $btnTexto = __('Guardar');
        return view('admin.cursos.editar')->with(compact('curso_', 'btnTexto')); // formulario actualizar curso
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(CursoRequest $cursoRequest, $slug)
    { 
        
        $curso_ = Curso::where('slug', $slug)->first();

        if(($cursoRequest->hasFile('image'))) {

            Storage::delete($curso_->imagen);
            $newimagen = FileHelper::uploadImage('image', 'images');
            $cursoRequest->merge(['imagen' => $newimagen]);
        }


        $curso_->fill($cursoRequest->input())->save();
        $cursos = Curso::paginate(5);
        return view('admin.cursos.ver')->with(compact('cursos', 'curso_')); //listado
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso=Curso::find($id);        
        $curso->DELETE(); // DELETE
        
        session()->flash('aviso', 'El curso ha sido eliminado correctamente.');
        session()->flash('alert-class', 'alert-danger'); 
        return back();
    }




    /**
     * Método que devuelve usuarios
     * @return mixed
     */
    public function getUsers()
    { 
        // Accedo a los usuarios y a su relación con cursos
        $users = User::with('cursos')->get();
        
        $actions = 'admin.datatables.inscritos_actions';
       
        return DataTables::of($users)->addColumn('actions', $actions)->rawColumns(['actions', 'cursos_formatted'])->make(true);

    }


    /**
     * Método que devuelve una vista con información de 
     * usuarios inscritos
     * @return mixed
     */
    public function viewInscritos(Curso $curso) 
    {
        $cursos = Curso::orderBy('titulo', 'ASC')->pluck('titulo', 'id');
        $users = User::where('id', '!=', auth()->user()->id)->with('cursos')->get();

        //dd($cursos); //usuarios inscritos;
        return view('admin.inscritos')->with(compact('cursos', 'users')); 
    }


    /**
     * Método que devuelve una vista con información de compras
     * @return mixed
     */    
    public function getCursosComprados() {

        $compras = DetalleCesta::join('cursos', 'cursos.id', 'detalle_cestas.curso_id')->groupBy('detalle_cestas.curso_id')->paginate(6);
//dd($compras);
        return view('admin.compras')->with(compact('compras')); 
    }
    

    /**
     * Método que cambia el estado del curso y permite ser visualizado
     * @return mixed
     */    
    public function publicar($slug) {
        $curso=Curso::where('slug', $slug)->first();
        $curso->estado = Curso::PUBLICADO;
        $curso->save();
        return back();
    }

}
