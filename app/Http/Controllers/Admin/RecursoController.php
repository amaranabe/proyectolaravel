<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tema;
use App\Recurso;
use App\Curso;
use App\Helpers\FileHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Session;

class RecursoController extends Controller
{
    
    // declarar variables de tipos de extensiones
    private $imagen_ext = ['jpg', 'jpeg', 'png'];
    private $audio_ext = ['mp3', 'ogg', 'mpga', 'wav', 'oga', 'spx', 'bin', 'dms', 'lrf', 'mar', 'so', 'dist', 'distz', 'pkg', 'bpk', 'dump', 'elc', 'deploy'];
    private $video_ext = ['mp4', 'mpeg', 'ogg', 'ogv', 'webm'];
    private $documento_ext = ['pdf'];


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Método que responde a una petición ajax y que devuelve todos los recursos
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAllRecursos(Request $request) {

    	$tema_id = $request->tema_id;

    	$data = Recurso::join('temas', 'temas.id', 'recursos.tema_id')
    		->where('recursos.tema_id', $tema_id)->select('recursos.*')->get();

    	return response()->json($data);
    }

    //método que envía una vista
    public function create() {

    	return view('admin.partials.modal.add_recurso');
    
    }

    /**
     * Método que responde a una petición ajax y que devuelve un recurso determinado por el id del recurso
     * @param  [integer] $id [id del recurso]
     * @return [json]     [description]
     */
    public function getRecurso($id) {
        
        $recurso=Recurso::find($id);
        return response()->json($recurso);
    }

    /**
     * Método que guarda un recurso en la BD
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request) {

    	$all_ext = implode(',', $this->allExtensions());

       
        $validator = Validator::make($request->all(), [
            'title' => 'required | min:5',
            'myfile' => 'required | file | mimes:' . $all_ext
        ]);

    	if ($validator->fails()) {
    		return response()->json(array('errors' => $validator->getMessageBag()));
		}

        //dd($request->file('myfile')->getClientOriginalExtension()); //consultar tipo de un fichero
    	$file = FileHelper::uploadFile('myfile', 'medias');

    	$recurso = new Recurso();
    	$recurso->tema_id = $request->tema_id;
    	$recurso->titulo = $request->title;
    	$recurso->file = $file;
    	$recurso->size = $request->file('myfile')->getClientSize();
        $recurso->extension = $request->file('myfile')->getClientOriginalExtension();
        $recurso->mimetype = $request->file('myfile')->getMimeType();
        $recurso->tipo = $this->getType($recurso->extension);
    	$recurso->save();

        return response()->json($file);
    
    }

    /**
     * Método que actualiza un recurso en la BD
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'title' => 'required | min:5',
            'myfile' => 'sometimes'
        ]);

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()));
        }

        $id = $request->recurso_id;
        $recurso=Recurso::find($id);
        //$newfile;
        if(($request->hasFile('myfile'))) {
            try {
                Storage::delete($recurso->file);
                $newfile = FileHelper::uploadFile('myfile', 'medias');
                $recurso->file = $newfile;
                $recurso->size = $request->file('myfile')->getClientSize();
                $recurso->extension = $request->file('myfile')->getClientOriginalExtension();
                $recurso->tipo = $this->getType($recurso->extension);
                
            } catch(PostTooLargeException $e) {
                return "Failed";
            }
        }
        $recurso->titulo = $request->title;
        $recurso->save();       
        return response()->json($recurso->file);
    }


    /** Función que elimina un Recurso
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function destroy (Request $request) {
        //dd($request);
    	$id = $request->id;
        $recurso=Recurso::find($id);  
        Storage::delete($recurso->file);

        $recurso->DELETE();
        return response()->json($id);
    }


    /**
     * Método que descarga un fichero ()
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getFile(Request $request) {

        $recurso=Recurso::find($request->id);
        $path=$recurso->file;
        return response()->download(storage_path('uploads/' .$path), null, [], null);

    }

    /**
     * Get type by extension
     * @param  string $ext Specific extension
     * @return string      Type
     */
    private function getType($ext)
    {
        if (in_array($ext, $this->imagen_ext)) {
            return 'imagen';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->documento_ext)) {
            return 'documento';
        }
    }

    /**
     * Get all extensions
     * @return array Extensions of all file types
     */
    private function allExtensions()
    {
        return array_merge($this->imagen_ext, $this->audio_ext, $this->video_ext, $this->documento_ext);
}


}
