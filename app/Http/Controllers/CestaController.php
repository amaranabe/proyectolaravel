<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Cesta;
use App\DetalleCesta;
use Session;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class CestaController extends Controller
{
    /**
     * Mostrar la cesta
     * @return [type] [description]
     */
    public function index()
    {
       if(!auth()->user()) {
       		//recogemos la cesta desde la sesión sesión
       		$detallesCesta= session()->get('cesta_anonima');
       } else {
          // recogemos todos los detalles que contiene la cesta
       		$detallesCesta = auth()->user()->cesta->detalles;
       }
       $total = $this -> total();
       return view('cesta', compact('detallesCesta', 'total'));
    }


    /**
     * Método que calcula el total de la cesta
     * @return [type] [description]
     */
    private function total() {
        $total = 0;
        if(!auth()->user()) {        
            $cesta_anonima = session()->get('cesta_anonima');
            if ($cesta_anonima !== null) {
              foreach ($cesta_anonima as $detalle) {
                  $total += $detalle->getTotal();                
              }
            }
       } else {
            // recogemos todos los detalles que contiene la cesta
            $detallesCesta = auth()->user()->cesta->detalles;
            foreach ($detallesCesta as $detalle) {
                $total += $detalle->getTotal();                
            }
       }
       return $total;
    }


    /**
     * Método que inicia el proceso de compra
     * @return [type] [description]
     */
    public function checkout() {

        $detalleCesta = new DetalleCesta();

        //Si no estoy registrado, actualizamos cesta BBDD con la cesta_anonima
        if(session()->has('cesta_anonima')) {
            $ca = session()->get('cesta_anonima');
            foreach ($ca as $item) {
                //recogemos en la BBDD  el id cesta
                $detalleCesta->cesta_id = auth()->user()->cesta-> id;
                               
                //compruebo si curso de cesta anonima existe en todos los detalles de cesta de la BD
                $existeCurso = DetalleCesta::where('curso_id', '=', $item-> curso_id)
                    ->first(); // devuelve null si no está       

                if($existeCurso == null) {
                    $detalleCesta->curso_id = $item-> curso_id;
                    $detalleCesta->precio = $item-> precio;     
                    $detalleCesta->precio = $item-> descuento;
                    $detalleCesta->save();

                } else {
                    //Si no es null, guardo mensajes de aviso de qué cursos ya están escritos
                    $cursos_inscritos[]= $existeCurso -> curso -> titulo;
                }
            }
            session()->forget('cesta_anonima');
        }

        $detallesCesta = auth()->user()->cesta->detalles;
        //compruebo si la lista de detalles cesta está vacía, para que no vaya a checkout
        if (count($detallesCesta)>0) {
            $total = $this -> total();
            if(!empty($cursos_inscritos)) {
              session()->flash('alertas', $cursos_inscritos);
            }
            return view('checkout', compact('detallesCesta', 'total'));
        }
        $alerta = "En los cursos seleccionados ya estás inscrito.";
        session()->flash('alerta', $alerta);
        return view('cesta', compact('detallesCesta'));
    }




    /**
     * Se procede a realizar el pago actualizando el estado y la fecha de la cesta y la tabla curso_user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payment(Request $request) {
        //accedemos a la pasarela de pago
        try {
          // Set your secret key: remember to change this to your live secret key in production
            Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token = $request-> stripeToken;

            $charge = Charge::create(array(
                'amount' => $this->total()*100,
                'currency' => 'eur',
                'source' => $request->stripeToken,
                'description' => "Compra curso",
                'receipt_email' => $request->email,
            ));
            //return 'Charge successful, you get the course!';
        } catch (\Exception $ex) {
            return back() -> withErrors('¡Error!' . $ex->getMessage());
        }

        //accedemos a los cursos
        $cursos= auth()->user()->cesta->detalles;
        foreach($cursos as $curso) {
            $this->inscripcion($curso-> curso_id, auth()->user()->id); //insertamos en tabla
        }

        //accedemos a la cesta del usuario
        $cesta= auth()->user()->cesta; 
        $cesta->fecha_pedido = new DateTime;
        $cesta->estado = 'finalizado';
        $cesta->save(); // UPDATE

        //enviamos un mensaje a través de la variable de sesión Flash
        $mensaje="Compra realizada con éxito. ¡ Gran elección !";
        return redirect('miscursos')->with(compact('mensaje'));
    }



    /**
     * Vaciar la cesta
     * @return [type] [description]
     */
    public function trash() {
        if(!auth()->user()) {
          //vaciamos la sesión
          session()->forget('cesta_anonima');
       } else {
          //recogemos todos los detalles que contiene la cesta
          $detallesCesta = auth()->user()->cesta->detalles;
          foreach ($detallesCesta as $item) {
                $item->delete();
          }           
       }       
       return back();
        
    }


    /**
     * Insertar datos en la tabla Curso_User
     * (asigna un curso a un usuario tras comprarlo)
     * @return [type]    [description]
     */
    public function inscripcion($c, $u) {
      //ingresar datos en la tabla curso_user
      \DB::table('curso_user')->insert([
        'curso_id' => $c, 
        'user_id' => $u
      ]);
    }
}
