<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetalleCesta;
use Session;


class DetalleCestaController extends Controller
{

    protected $countItems;


    public function __construct() {

        if(!auth()->user()) {
            if(!session() -> has('cesta_anonima')) {
            session() -> put('cesta_anonima', array());
            }
        }

    }


    /**
     * Almacenar curso en cesta
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    protected function store(Request $request) 
    {
        // Creo nuevo objeto DetalleCesta
        $detalleCesta = new DetalleCesta();

        // Comprobar si usuario ha iniciado sesión
        if(!auth()->user()){

            // Recupero array de la session cesta abierta
            $cesta_anterior = $request->session()->get('cesta_anonima');

            //Compruebo si curso existe ya en la cesta anonima
            $key = array_search($request-> curso_id, array_column($cesta_anterior,'curso_id'));
            if($key !== false) {
                $aviso = "El curso ya estaba añadido a la cesta.";
                return back() ->with(compact('aviso'));
            }

            $detalleCesta->cesta_id = session()->getId();        
            $detalleCesta->curso_id = $request->curso_id;
            $detalleCesta->precio = $request->precio;
            $detalleCesta->descuento = $request->descuento;
            // añado nuevo objeto detalleCesta a la sesión cesta_anonima
            // (no necesito comprobar si hay sesion creada porque la he creado en el constructor)
            $cesta_anterior[] = $detalleCesta;
            // sobreescribe en la session
            $request->session()->put('cesta_anonima', $cesta_anterior);                 
        } 
        else {

            //sólo va a haber una cesta activa por cada usuario --> por tanto, cómo obtenemos el id de la cesta activo, de un determinado cliente
            //creamos en la BBDD  el id cesta donde queremos registrar este detalle. (llamar del usuario registrado)
            $detalleCesta->cesta_id = auth()->user()->cesta-> id;
            $detalleCesta->curso_id = $request-> curso_id;
            $detalleCesta->precio = $request-> precio;
            $detalleCesta->descuento = $request->descuento;
            $detalleCesta->save();

        }

        $aviso = "El curso se ha añadido a tu cesta correctamente.";
    	return back() ->with(compact('aviso'));
    }


    /**
     * Eliminar elemento de cesta
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request) {

        if(!auth()->user()) {
            
            $cesta_anonima= session()->get('cesta_anonima'); //recogemos la cesta de sesión
            $posicion= $request -> detalle_cesta;
            unset($cesta_anonima[$posicion]); //eliminamos el elemento, conocientdo su posicion
            session()->put('cesta_anonima', $cesta_anonima); //actualizamos cesta

        } else {
            //buscamos el id del detalle a eliminar, pasandole el campo donde hemos definido el id (name)

            $detalleCesta = DetalleCesta::find($request-> input('detalle_cesta_id'));
            //para evitar la vulnerabilidad de que cualquier usuario pueda borrar elementos de las cestas de otros usuarios, hacemos una comprobación
            if($detalleCesta -> cesta_id == auth()->user()->cesta-> id) {
                $detalleCesta->delete();
            }

        }     

        $aviso = "El curso se ha eliminado de la cesta correctamente.";
        return back()->with(compact('aviso'));
    }


}
