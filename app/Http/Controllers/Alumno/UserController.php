<?php

namespace App\Http\Controllers\Alumno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Rules\ValidCurrentPassword;
use Validator;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        /**
         * fetching the user model
         **/
        $user = Auth::user();

        /**
         * Passing the user data to profile view
         */
        return view('alumnos.misdatos', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update()
    { 
        //personalizo los mensajes de validación
        $message = [
            'name.required' => 'El campo Nombre es requerido',
            'apellido1.alpha' => 'El campo Primer Apellido sólo debe contener letras',
            'apellido2.alpha' => 'El campo Segundo Apellido sólo debe contener letras',
        ];

        //Valido mediante una de las facilidades de laravel 5.5, usando el helper request() y le paso los mensajes
        request()->validate([
            'name' => 'required',
            'apellido1' => 'nullable|alpha',
            'apellido2' => 'nullable|alpha',
            'email' => 'required|email',
        ], $message);

        // Recojo los datos de la petición
        $data = request()->all();

        //Obtengo el modelo usuario
        $user = Auth::user();

        // Asigno a user los datos de la petición
        $user->fill($data);
        // guardamos los datos
        $user->save();

        // Enviamos a la vista
        request()->session()->flash('profile_message', '¡Tus datos han sido correctamente actualizados.!');
        return back();
    }

    /**
     * Change the password for the user
     * @param Request  $request
     * @return Response
     */
    public function updatePassword(Request $request)
    {
        
        //Definir las reglas de validación
        $rules = [
            'password' => ['required', 'string', new ValidCurrentPassword],
            'newpassword' => 'required|string|different:password|confirmed|min:6',
            'newpassword_confirmation' => 'required_with:newpassword',
        ];

        //Personalizar los mensajes de error
        $message = [
            'newpassword.different' => 'La nueva contraseña no puede coincidir con la anterior.',
            'newpassword.confirmed' => 'La confirmación de la nueva contraseña no coincide.',
            'newpassword.min' => 'La nueva contraseña debe contener al menos 6 caracteres.'            
        ];


       // Hacer la validación y redireccionar a la vista los errores cuando falla la validación  
       $validator = Validator::make($request->all(), $rules, $message);
       if($validator->fails()) {
            return redirect('misdatos/editar')->with(compact($request))
                        ->withErrors($validator, 'password_div')
                        ->withInput();
        }

        //Validación pasada, guardar la nueva password
        $user = Auth::user();
        $user->password = bcrypt(request()->input('newpassword'));
        $user->save(); // guardar
        request()->session()->flash('profile_message', '¡Tu contraseña ha sido correctamente actualizada!');
        return back();
    }



    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user=auth()->user();
        $user->delete();
        request()->session()->flash('header_message', 'Su cuenta está dada de baja definitivamente. Recuerde, que puede volver a registrarse cuando lo desee.');
        return redirect('/');
    }



}
