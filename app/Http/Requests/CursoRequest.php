<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CursoRequest extends FormRequest
{
    
    protected $errorBag = 'curso_form';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array (con las reglas de valicación aplicadas)
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'titulo' => 'required|min:5',
                    'titulo2' => 'required|min:5',
                    'categoria_id' => [
                        'required',
                        Rule::exists('categorias', 'id'), 
                    ],
                    'duracion' => 'required|numeric|min:0',
                    'precio' => 'required|numeric|min:0',
                    'descuento' => 'required|numeric|min:0',
                    'image' => 'required|image|mimes:jpg,jpeg,png|max:1999',
                    'dirigido_a' => 'required|max:250',
                    'objetivos.0' => 'required_with:objetivos.1'
                ];
            }
            case 'PUT':{
                return [
                    'titulo' => 'required|min:5',
                    'titulo2' => 'required|min:5',
                    'categoria_id' => [
                        'required',
                        Rule::exists('categorias', 'id'), 
                    ],
                    'duracion' => 'required|numeric|min:0',
                    'precio' => 'required|numeric|min:0',
                    'descuento' => 'required|numeric|min:0',
                    'image' => 'sometimes|image|mimes:jpg,jpeg,png|max:1999',
                    'dirigido_a' => 'required|max:250',
                    'objetivos.0' => 'required_with:objetivos.1'
                ];
            }
        }
    }
}
