<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;
use Hash;

/**
 * Clase que permite crear una regla de validación
 * Funcion: comprueba que la password actual coincide con la del usuario registrado.
 */
class ValidCurrentPassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * DEFINE LA LÓGICA QUE VA A TENER
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //Comprobar si el password actual y el registrado son iguales
        if(Hash::check($value, auth()->user()->password)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tu password actual no coincide con el password registrado.';
    }
}
