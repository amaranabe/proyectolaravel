<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordNotification;

use App\Curso;
use App\Cesta;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'apellido1', 'apellido2', 'email', 'password', 'admin', 'picture', 'confirmado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $appends = ['cursos_formatted'];

    /**
     * Send the password reset notification (from
     * Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification).
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token, $this->name));
    }



    /**
     * 
     */
    public static function navigacion() {
        if(auth()->check()){
            return auth()->user()->admin;
        }
        return 'guest';
    }

    /**
     * Método que crea relación entre los modelos Usuario y Cesta
     * 
     * @return [Cesta] [cestas asociadas al usuario, incluido los pedidos]
     */
    public function cestas()
    {
        return $this->hasMany(Cesta::class);
    }

    /**
     * Método que crea relación directa con modelo Curso.
     * Permite mantener así la integridad referencial de la BD.
     *
     * belongsToMany nos permite trabajar con relaciones donde muchos
     * registro pertenece a otros muchos (Define la relación muchos a 
     * muchos ->Un usuario va a tener uno o más cursos)
     * @return [type] [description]
     */
    public function cursos() {
        return $this->belongsToMany(Curso::class);
    }

   
    /**
     * Método que crea un acceso (accesor) al modelo Cesta que me devuelva
     * un objeto Cesta 'activo', con todos sus atributos, el cuál se llama
     * automáticamente cuando se accede a su modelo Cesta.
     *
     * @return [Cesta]
     */
    public function getCestaAttribute()
    {
        //obtenemos la primera coincidencia (first()) de una cesta cuyo estado sea activo
        $cesta = $this->cestas()->where('estado', 'activo')->first();
        if($cesta) {
            return $cesta;
        }

        //Si no creamos una nueva cesta
        $cesta = new Cesta();
        $cesta->estado = "activo";
        $cesta->user_id = $this-> id;
        $cesta->save();

        return $cesta;
    }

    /**
     * Devuelve true si el usuario está verificado 
     * 
     * @return boolean [description]
     */
    public function verified() {
        return $this->confirmation_code === null;
    }


    public function getCursosFormattedAttribute() {
        return $this->cursos->pluck('titulo')->implode('<br />');
    }


    
}
