<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use APP\Curso;
use APP\User;

class Rating extends Model
{
    protected $fillable = ['curso_id', 'user_id', 'valoracion', 'comentario'];

    public function curso() {
    	return $this->belongsTo(Curso::class);
    }

	public function user() {
		return $this->belongsTo(User::class);
	}
}
