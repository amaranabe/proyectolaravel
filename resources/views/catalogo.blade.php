@extends('layouts.master')
@section('description', 'Listado de cursos que ofrecen herramientas para apoyo a persona con discapacidad intelectual')
@section('title', 'Catálogo Cursos')
@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __(" Cursos"),
        "icono" => "book",
        "query" => "Hay una variedad de cursos destinados a brindar apoyo a personas con discapacidad"
    ])
@stop

@section('contenido')
<section class="container wrapper-border">
    <div>
        <div class="row">
            <!-- Mensaje de aviso -->
            <div class="col-md-12 mb-3">
                @if(session('aviso'))
                    <div class="">
                        <div class="alert alert-success text-center">
                            {{ session('aviso') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>  

            <!-- Listado de cards de cada curso disponible-->
            @forelse($cursos as $curso)
                <div class="col-md-4">
                    @include('partials.card')
                </div>
            @empty
                <div class="alert alert-danger">
                    {{ __("No hay ningún curso disponible") }}
                </div>
            @endforelse
        </div>
    </div>
</section>
@stop

