@extends('layouts.master')
@section('description', 'Formulario para completar proceso de compra')
@section('title', 'Checkout')
@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __("Compra"),
        "icono" => "cc-stripe",
        "query" => "Finaliza tu compra"
    ])
@section('contenido')
<div class="container wrapper-border">

	<main class="row d-flex justify-content-center mb-5">
		<div class="col-md-5">
			<div class="lista-cesta">
				@if(session('alertas'))
					@foreach (session('alertas') as $alert)
						<div class="alert alert-success">
							{{ "Ya estás inscrito en el curso '" . $alert . "'." }}
						</div>
					@endforeach
				@endif				
				<p>Tus cursos ({{ count($detallesCesta) }})</p>
				<div class="table-responsive">
					<table class="table">
						  <thead>
						  </thead>
						  <tbody>
						    @foreach($detallesCesta as $detalle)
						    	<tr>
							      <td class="w-25"><img src="{{ asset('/storage/' . $detalle->curso -> imagen) }}" alt="Responsive image" width="150"></td>
							      <td class="w-50">{{ $detalle -> curso-> titulo }}</td>
							      <td class="w-25">{{ number_format($detalle ->getTotal(), 2, '.', ',') }} €</td>
							    </tr>
						    @endforeach
						    <tr><td colspan="3"><h4>Total: {{ number_format($total, 2, '.', ',') }} €</h4></td></tr>
						  </tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-5 ml-5">
			<div class="card">
			  <div class="card-body">
			    <h5 class="card-title">Formulario pago</h5>
			    @include('partials.payment_form')				
			  </div>
			</div>
		</div>
	</main>
</div>

@stop

{{-- Calling Stripe Library --}}
@section('scripts_in_views')

	<script type="text/javascript">
		$(function() {

				//create an instance of Elements
				var stripe = Stripe('pk_test_eyafAxJCTNACFmXeKUOqcqFd');
				var elements = stripe.elements();


				// Custom styling can be passed to options when creating an Element.
				var style = {
				  base: {
				    // Add your base input styles here. For example:
				    fontSize: '16px',
				    color: "#32325d",
				  }
				};

				// Create an instance of the card Element.
				var card = elements.create('card', {
					style: style,
					hidePostalCode: true

				});

				// Add an instance of the card Element into the `card-element` <div>.
				card.mount('#card-element');
				/** 
				 * El Elemento de la card simplifica la forma y minimiza el número de campos requeridos insertando un único campo de entrada flexible que recopila de forma segura todos los detalles necesarios de la tarjeta. Consulte nuestra documentación de referencia de Stripe.js para obtener una lista completa de los tipos de elementos que son compatibles.

				Los elementos validan la entrada del usuario a medida que se escribe. Para ayudar a sus clientes a detectar errores, debe escuchar los eventos change en el Elemento de la card y mostrar los errores:
				 * @param  {[type]} event) {             var displayError [description]
				 * @return {[type]}        [description]
				 */
				card.addEventListener('change', function(event) {
				  var displayError = document.getElementById('card-errors');
				  if (event.error) {
				    displayError.textContent = event.error.message;
				  } else {
				    displayError.textContent = '';
				  }
				});

				// // Create a token or display an error when the form is submitted.
				// Handle form submission.
				var form = document.getElementById('payment-form');
				form.addEventListener('submit', function(event) {
				  event.preventDefault();


				  var options = {
				  	name: document.getElementById('fullname').value
				  }

				  stripe.createToken(card, options).then(function(result) {
				    if (result.error) {
				      // Inform the customer that there was an error.
				      var errorElement = document.getElementById('card-errors');
				      errorElement.textContent = result.error.message;
				    } else {
				      // Send the token to your server.
				      stripeTokenHandler(result.token);
				    }
				  });
				});


				//Submit the token and the rest of your form to your server
				function stripeTokenHandler(token) {
					// Insert the token ID into the form so it gets submitted to the server (Insertar el token ID en el formulario para que sea enviado al servidor)
					var form = document.getElementById('payment-form');
					var hiddenInput = document.createElement('input');
					hiddenInput.setAttribute('type', 'hidden');
					hiddenInput.setAttribute('name', 'stripeToken');
					hiddenInput.setAttribute('value', token.id);
					form.appendChild(hiddenInput);

					// Submit the form
					form.submit();
				}
		});
	</script>
@stop