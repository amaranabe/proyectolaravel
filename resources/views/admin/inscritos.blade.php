@extends('admin.layouts.master')

@section('title', 'Panel Administración')
@section('breadcrumbs', Breadcrumbs::render('usuarios'))
@section('contenido')

    <!-- Panel principal de información -->
    <div class="row">
        <div class="col-sm-12">
          <div class="card m-5">
            <div class="card-header">
                <h4>{{ __('Usuarios') }}</h4>
            </div>
            <div class="card-body d-flex justify-content-center">
              	<div class="col-sm-10 info_panel table-responsive">
                  <!-- Usuarios inscritos -->
                  <table 
                    class="table table-bordered nowrap mt-2"
                    cellspacing="0"
                    id="tabla-alumnos"
                  >
                      <thead>
                        <tr>
                          <th class="text-center" scope="col">{{ __('#') }}</th>
                          <th scope="col">{{ __('Nombre') }}</th>
                          <th scope="col" >{{ __('Apellido') }}</th>
                          <th scope="col" >{{ __('Email') }}</th>
                          <th scope="col" >{{ __('Cursos') }}</th>
                          <th scope="col" >{{ __('Opciones') }}</th>
                        </tr>
                      </thead>

                  </table>                  
				        </div>
            </div>
          </div><!-- / card content -->
        </div>
    </div>
@stop

@push('scripts')
<script type="text/javascript">
        var data='';
        var modal=$('#sendEmail');
      $(document).ready(function() {

          $('#tabla-alumnos').DataTable({
                "pageLength": 10,
                "lengthMenu": [5, 10, 25 ],
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('inscritos') }}",
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }, 
                "columns": [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'apellido1'},
                    { data: 'email' },
                    { data: 'cursos_formatted' },
                    { data: 'actions' },
                ]
            });

          $(document).on('click', '.btnEmail', function(e) {
              e.preventDefault();
              var id = $(this).data('id');
              modal.find('.modal-title').text('{{ __("Enviar mensaje") }}');
              modal.find('#modalAction').text('{{ __('Enviar mensaje') }}').show();
              modal.modal();
          });

        });

</script>
@endpush
