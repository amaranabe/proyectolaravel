@extends('admin.layouts.master')

@section('title', 'Panel Administración')
@section('breadcrumbs', Breadcrumbs::render('editar', $curso_))
@section('contenido')

<main>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="card mt-3">
					<div class="card-header d-inline-flex">
						<h4>{{ __('Editar curso') }}</h4>

					</div>

					<div class="card-body">								
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach		
								</ul>
							</div>
						@endif

					@include('admin.partials.curso_form', [
						'method' => 'PUT',
					])
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@stop