@extends('admin.layouts.master')

@section('title', 'Panel Administración')
@section('breadcrumbs', Breadcrumbs::render('ver', $curso_))
@section('contenido')
<!-- Mensaje de notificación -->

<div class="container-fluid">
<!-- cabecera de mensajes -->
@if(session('mensaje'))
	<div class="col-12 p-0">
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    	</div>
    </div>
@endif

	<main>	
			<div class="row">
				<div class="col-md-5">
					<div class="card mt-3">
						<div class="card-header d-inline-flex">
							<h4>{{ __('Información del curso') }}</h4>
							<a class="btn btn-outline-primary ml-auto" href="{{ route('cursos.editar', $curso_->slug) }}">
			                	<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;{{ __('Editar') }}
							</a>
						</div>
						<!-- Ver sólo información del curso -->
						<div id="disable-form" class="card-body">							
						@include('admin.partials.curso_form', [
								'method' => 'GET',
							])
						</div>
					</div>
				</div>

				<div class="col-md-7">
					<!-- Ver Temas -->
					<div class="card mt-3">
						<div class="card-header d-inline-flex">
							<h4>{{ __('Temas') }}</h4>
							<a class="btn btn-outline-success ml-auto" href="#" role="button" data-toggle="modal" data-target="#addTema">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;{{ __('Tema') }}
							</a>
						</div>
						<div class="card-body text-sm-center">
							@if(Session::has('aviso'))
								<div class="text-left alert {{ session('alert-class') }}">
									{{Session::get('aviso')}}
								</div>
							@endif

							@if(count($curso_->temas)>=1)
							    @include('admin.partials.tabla_temas')
							@else
							    <h6>No hay temas disponibles</h6>
							@endif
						</div>
					</div>

					<!-- Modal add Tema -->
					@include('admin.partials.modal.add_tema')

					<!-- Ver Recursos -->
					<div class="card mt-3">
						<div class="card-header d-inline-flex">
							<h4>{{ __('Recursos') }}</h4>
							<div id="header-button" class="ml-auto"></div>
						</div>
						<div class="card-body text-sm-center">
							<div class="flash-message">
								@include('admin.partials.flash_messages')
							</div>
							@include('admin.partials.select_form_temas')

							<!-- Ver recursos -->
							<div class="card text-left d-none">
								<div id="recursos_div" class="card-body"></div>
							</div>
						</div>
					</div>

					<!-- Modal add and edit Recurso -->
					@include('admin.partials.modal.add_recurso')
					@include('admin.partials.modal.edit_recurso')
				</div>
			</div>
	</main>

	<section>
		<div class="container-fluid p-0">
			<div class="row">
	        <div class="col-sm-12">
	          <div class="card mt-5">
	            <div class="card-header d-inline-flex">
	                <h4>{{ __('Ver cursos') }}</h4>
	            </div>
	            <div class="card-body">
	              	<div class="info_panel">
						<div>
							@include('admin.partials.tabla_cursos')
							<div class="mt-2 d-flex justify-content-lg-center">	{{ $cursos -> links () }} </div>
						</div>
					</div>
	            </div>
	          </div><!-- / card content -->
	        </div>
	    	</div>
	    </div>
	</section>
</div>
@stop


@push('scripts')
<script type="text/javascript">


/**
 * Display a file in new window
 * @return {[type]} [description]
 */
function openFileInWindow(id) {
	
	var url= "{{ url('/admin/cursos/temas/recurso') }}/" + id;
	console.log(url);
	$.ajax({
		type: "POST",
        dataType: "html",
        url: url,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data: {
        	"id": id,
        	"_token": "{{ csrf_token() }}"
        },
        success: function(response){
        	console.log('ok');
        }

	});
}

$(document).ready(function () {
  		$("body").tooltip({
		    selector: '[data-toggle="tooltip"]'
		});

  		// desabilitar formulario
  		$("#disable-form :input").attr("disabled", true);

  		// modificar posicion
  		$(".up,.down").click(function(){

	      	//si posicion es string vacío es que estamos en los extremos, no tiene ese elemento
	        //row actual
	        var current_row = $(this).parents("tr:first");
	        var current_posicion = current_row.find("td:eq(1)").text();
	        //row previa
	        var prev_row = current_row.prev();
	        var prev_posicion = prev_row.find("td:eq(1)").text();
    		//row siguiente
	        var next_row = current_row.next();
	        var next_posicion = next_row.find("td:eq(1)").text();

	        //aplicar
	        if ($(this).is(".up")) {
	        	//controllar posición de la primera fila
	        	if(prev_posicion != "") {
	        		//actualizar valores
	        		current_row.find("td:eq(1)").text(prev_posicion);
	        		prev_row.find("td:eq(1)").text(current_posicion);

	        		//mover la fila a su posición
	        		current_row.insertBefore(prev_row);
	        		
	        	}
	        } else {
	        	//controlar posición de la última fila
	        	if(next_posicion != "") {
	        		//actualizar valores de la columna td[1]
	        		current_row.find("td:eq(1)").text(next_posicion)
					next_row.find("td:eq(1)").text(current_posicion);

	        		//mover la fila a su posición
	        		current_row.insertAfter(next_row);
	        	}
	        }

	        //preparar valores a enviar
	        var temas = new Array();

	        $('#tabla_temas tr').each(function() {

	        	var cell = $(this).find("td:eq(1)");
	        	var id = cell.data('tema');
	        	if(id>=1) {
	        		temas[id] = cell.text();
	        	}
	        });
  			console.log(temas);

	        // Update posicion value from storage
	        var current_id = current_row.find("td:eq(1)").data('tema'); //id tema actual
	        var prev_id = current_row.find("td:eq(1)").data('tema');
			$.ajax({
			  	type: "POST",
			  	url: "{{ route('temas.posicion') }}",
			  	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	        	data: { 
	            	"temas": temas,
	            	"_token": "{{ csrf_token() }}"
	        	},
			  	success: function(response) {
			    	
			  	},
			  	error: function(response) {
			  		alert('Ha ocurrido un error. Vuelva a intentarlo.');
			  	}
			});

	    });




//mostrar recursos - COMBOBOX TEMA
$("select#titulosTemas").change(function(e) {

	e.preventDefault();
	$("#recursos_div").empty();
	$('div.flash-message').empty();
    var tema = $("#titulosTemas option:selected").val();

    // Get recursos from tema_id
    if(tema !='default') {
    	$.ajax({
          	type: "GET",
          	dataType: "html",
          	url: "{{ route('recursos.getRecursos') }}",
          	data: {'tema_id': tema},
          	success: function(response){
          		var buttonAddMedia = '<a class="btn btn-outline-secondary" href="#" role="button" data-toggle="modal" data-target="#addMedia" data-id="' + tema + '" id="addRecurso">' + '<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Añadir recurso</a>';


            	$("#recursos_div").parent().attr('class', 'card text-left d-block');

          		if ($.isEmptyObject(JSON.parse(response))) {
						var sinrecursos = '<div class=text-center><h5>No hay recursos</h5></div>';
          				$("#recursos_div").append(sinrecursos);

          		} else {
          			var tabla ='<table id="tablaRecursos" class="table">'+
							  '<thead>' +
							    '<tr>' +
							      '<th scope="col">Recurso</th>' +
							      '<th scope="col">Tipo</th>' +
							      '<th scope="col">Ext.</th>' +
							      '<th class="text-right" scope="col">Opciones</th>' +
							    '</tr>' +
							  '</thead>'
							  '<tbody></tbody>'+
							'</table>';

					$("#recursos_div").append(tabla);
          			$.each(JSON.parse(response), function(i, recurso){
          				
						var deleteBtn = '<button id="delete_recurso" data-toggle="tooltip" data-placement="bottom" title="Eliminar recurso" class="btn btn-danger btn-xs mr-1" value="'+ recurso.id +'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';

						var editBtn = '<span id="editRecurso" data-toggle="modal" data-target="#editMedia" data-id="'+ recurso.id +'"><button data-toggle="tooltip" data-placement="bottom" title="Editar recurso" class="btn btn-info btn-xs mr-1"><i class="fa fa-edit" aria-hidden="true"></i></button></span>';


          				$('#tablaRecursos').append($("<tbody/>")); 
          				var row = $('<tr>' +
          							'<td>' + 
          								'<a href="{{ url('/admin/cursos/temas/recurso') }}/' + recurso.id+'" class="btn btn-link">' +recurso.titulo +'</a>'+ '</td>' +
          							'<td>' + recurso.tipo + '</td>' +
          							'<td>' + recurso.extension + '</td>' +
          							'<td class="text-right">' + editBtn + deleteBtn + '</td>' +
          							'</tr>'
          					);
          				$('#tablaRecursos > tbody:last').append(row);
          				$('#delete_recurso').tooltip();
          				$('#editRecurso').tooltip();
          				$('#showFile').attr('href', '{{ url('admin/cursos/') }}/' + recurso.file);

      				});
          		} // fin else
          		$("#header-button").html(buttonAddMedia); //button añadir recurso
				    
          	} //fin success
    	}); //fin ajax
	} else {
		//no mostrar div
		$("#recursos_div").parent().attr('class', 'card text-left d-none');
		$("#header-button").empty();
	}
});

//mostrar formulario modal crear tema 
$('#addTema').on('show.bs.modal', function (e) {

	//Vaciar campos anteriores
	$("input[name='title']").removeClass('is-invalid');
    $("textarea").removeClass('is-invalid');


});

//mostrar formulario modal editar tema 
$('#editTema').on('show.bs.modal', function (e) {

	//Vaciar campos anteriores
	$("input[name='title']").removeClass('is-invalid');
	$("textarea").removeClass('is-invalid');
    

});

//botón que muestra formulario modal para crear recurso con el tema_id
$('#addMedia').on('show.bs.modal', function (e) {

	//Vaciar campos anteriores
	$("input[name='title']").removeClass('is-invalid');
	$("input[name='title']").val('');
    $(".errorTitle").text();
    $("input[name='myfile']").removeClass('is-invalid');
    $("label[for='myfile']").text('{{ __('Selecciona fichero') }}');
    $(".errorFile").text();

	  var modalButton = $(e.relatedTarget); // span that triggered the modal
	  var id = modalButton.data('id');
	  var modal = $(this);
	  modal.find('.modal-body #tema_id').val(id);
});


//mostrar formulario modal con campos de recurso para editar
$('#editMedia').on('show.bs.modal', function (e) {

	//Vaciar campos anteriores
	$("input[name='title']").removeClass('is-invalid');
    $(".errorTitle").text();
    $("input[name='myfile']").removeClass('is-invalid');
    $(".errorFile").text();

	  var modalButton = $(e.relatedTarget); // span that triggered the modal
	  var id = modalButton.data('id');
	  console.log(id);
	  var url= "{{ url('/admin/cursos/temas/recursos') }}/" + id;
	  console.log(url);

	  $.ajax({
		  	url: url,
		  	method: 'GET',
		  	cache: false,
			contentType: false,
			processData: false,
			data: { 
		            "recurso_id": id, 
		        },
		    success: function(data) {
		    	console.log(data);
			  	$("input[name='recurso_id']").val(id);
			  	$("input[name='title']").val(data.titulo);
			  	$("label[for='myfile'] > small").text(data.file);
			  	$('#editMedia').modal('show'); 	
		    },
		    error: function (result) {
		    	// body...
		    }
	  });
});



//eliminar recurso y fichero
$(document).on('click', '#delete_recurso', function(e) {
    	e.preventDefault();
    	var idrec = $(this).val();
    	var row = this;

    	// Delete a recurso_id
	    $.ajax({
	        type: "DELETE",
	        dataType: 'json',
	        url: "{{ route('recursos.destroy') }}",
	        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	        data: { 
	            "id": idrec, 
	            "_token": "{{ csrf_token() }}"
	        },
	        success: function(result) {
	        	$(row).closest( "tr" ).remove();
	        	//$('#recursos_div').append('<p class="alert alert-success">' + result.success + '</p>');
	        	//$('div.flash-message').html(result);
	        	showRecursos(result);
	        	
	        	
	        }
	    });
});




	    // ver fichero en el navegador
	    $(document).on('click', '#showAjaxFile', function(e) {

        	e.preventDefault();

            var path = $(this).val();
            $.ajax({
                type: "POST",
                data: {
                	"file_path": path,
                	"_token": "{{ csrf_token() }}"
            	},
                url: "",
                success: function(response)
                {
                	console.log(response);

                },
                error: function (xhr, textStatus, errorThrown, response) 
			    {

			        console.log(xhr.status);
			        console.log(xhr.statusText);
			        console.log(xhr.textStatus);
			        console.log(xhr.errorThrown);
			    }
            });
        });  
        
  });
</script>
@endpush