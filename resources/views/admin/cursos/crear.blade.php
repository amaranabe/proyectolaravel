@extends('admin.layouts.master')

@section('title', 'Panel Administración')
@section('breadcrumbs', Breadcrumbs::render('crear'))
@section('contenido')

<div class="container">
	<!-- Mensaje de notificación -->
	@if(session('mensaje'))
		<div class="col-12 p-0">
		    <div class="alert alert-success alert-dismissible fade show" role="alert">
		        {{ session('mensaje') }}
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		    </div>
		</div>
	 @endif
	
	<main>	

		<!-- Formulario para curso -->
		<div class="card mt-3">
			<div class="card-header d-inline-flex">
				<h4>{{ __('Introduce información del nuevo curso') }}</h4>
			</div>

			<div class="card-body">
				<div class="row">
					<!-- campos curso -->							
					<div class="col-sm-12">
						@include('admin.partials.curso_form', [
							'method' => 'POST',
							'ruta' => 'cursos.store'
						])
					</div>
				</div>
			</div>
			
		</div>

	</main>

</div>
@stop

