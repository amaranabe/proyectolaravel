<div class="table-responsive">
	<table id="tabla_temas" class="table mt-2 table-hover">
  	<thead>
	    <tr>
	    	<th class="text-center" scope="col"></th>
	      <th class="text-center" scope="col">{{ __('Nº') }}</th>
	      <th class="text-left" scope="col">{{ __('Titulo') }}</th>
	      <th class="text-right" scope="col" >{{ __('Opciones') }}</th>
	    </tr>
		</thead>
  <tbody>
    @foreach($curso_->temas as $tema)
	    <tr>	
	      <td class="text-center">
	      	<div class="d-inline-flex text-dark">
	      		<button type="button" class="up btn btn-outline-secondary mr-1"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
	      		<button type="button" class="down btn btn-outline-secondary"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
	      	</div>    	
		  </td>
		  <td class="posicion" data-tema="{{ $tema->id }}" scope="row"><strong>{{ $tema->posicion }}</strong></td>
	      <td class="text-left active">{{ $tema -> titulo}}</td>
	      <td class="td-actions text-right">					
	 			<div class="d-inline-flex">
	 				<!--  to add a modal and a tooltip without adding javascript or altering the tooltip function, you could also simply wrap an element around it: Then it add an span-->
		      		<span data-toggle="modal" 
		      				data-id="{{$tema->id}}"
		      				data-title="{{$tema->titulo}}" 
		      				data-descripcion="{{$tema->descripcion}}"
		      				data-target="#editTema" 
		      				id="editTema">
		      			<a href="#" 
		      				data-toggle="tooltip"
		      				data-placement="bottom" 
		      				title="Editar tema" 
		      				class="btn btn-info btn-xs mr-1"
		      			><i class="fa fa-edit"></i></a>
		      		</span><!-- ./ button to edit Tema -->
		      		<form method="post" action="{{ url('/admin/cursos/temas/'. $tema -> id) }}">{{ csrf_field()}}
		 				{{ method_field('DELETE')}}
		 				<button type="submit" data-toggle="tooltip" data-placement="bottom" title="Eliminar tema" class="btn btn-danger btn-xs  mr-1"><i class="fa fa-trash-o" aria-hidden="true"></i></button><!-- ./ button to delete Tema -->
		 			</form>
	 				 <button data-toggle="tooltip" title="Ver recursos" data-placement="bottom" class="btn btn-outline-info btn-xs mr-1" onclick="showRecursos({{ $tema->id }})"><i class="fa fa-paperclip" aria-hidden="true"></i>
	 				 </button><!-- ./ button to show recursos -->
	 			</div>
	      </td>
	    </tr>
    @endforeach
    <!-- Modal edit Tema -->
@include('admin.partials.modal.edit_tema')
  </tbody>
</table>

</div>


@push('scripts')
	<script>
			function showRecursos(id) {
					var p ="#titulosTemas option[value='"+id+"']";
					$(p).prop('selected', true);
					$("#titulosTemas").trigger('change');
			}

		$(document).ready(function(){

		    $('#editTema').on('show.bs.modal', function (e) {

			  	var modalButton = $(e.relatedTarget); // span that triggered the modal
			  	var titulo = modalButton.data('title'); //Extract info from data-* attributes
			  	var descripcion = modalButton.data('descripcion');
			  	var id = modalButton.data('id');

			  	var modal = $(this);
			  	modal.find('.modal-body #tema_id').val(id);
			  	modal.find('.modal-body #title').val(titulo);
			  	modal.find('.modal-body #descripcion').val(descripcion);

			});

		    
		});
</script>
@endpush

