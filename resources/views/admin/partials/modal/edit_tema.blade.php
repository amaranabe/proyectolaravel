<!-- Modal para crear Tema -->

<div class="modal fade" id="editTema" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog">
    <!-- modal content -->
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <h5 class="modal-title" >{{ __('Editar un tema') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- modal body / form  -->
      <form method="POST" action="{{ route('temas.update', 'tema') }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="modal-body text-left">

            <input type="hidden" name="tema_id" id="tema_id" value="{{ $tema->id }}">

            <div class="form-group d-block mt-2">
              <label class="control-label" for="title">{{ __('Titulo') }}</label>
              <input type="text" class="form-control{{ $errors->edit_tema->has('title') ? ' is-invalid' : ''}}" id="title" name="title" value="{{ old('title') }}" />
              @if($errors->edit_tema->has('title'))
                  <small class="form-text invalid-feedback">{{ $errors->edit_tema->first('title') }}</small>
              @endif
            </div>

            <div class="form-group mt-2">
              <textarea class="form-control{{ $errors->edit_tema->has('descripcion') ? ' is-invalid' : ''}}" id="descripcion" rows="3" aria-describedby="descripcionHelp" name="descripcion"></textarea>
              <small id="descripcionHelp" class="form-text text-muted">{{ _('* Opcional.') }}</small>
              @if($errors->edit_tema->has('descripcion'))
                  <small class="form-text invalid-feedback">{{ $errors->edit_tema->first('descripcion') }}</small>
              @endif
            </div>
          </div>
        <!-- modal body / form  -->
          <div class="modal-footer">
              <button type="submit" class="btn btn-danger">{{ __('Guardar') }}
              </button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cerrar') }}</button>
          </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')

  @if(count($errors->edit_tema)>0)
  <script>
      $(document).ready(function(){ 
          $('#editTema').modal({show: true});
      });
  </script>
  @endif

@endpush