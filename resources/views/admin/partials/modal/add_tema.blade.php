<!-- Modal para crear Tema -->
<div class="modal fade" id="addTema" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog">
    <!-- modal content -->
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <h5 class="modal-title" >{{ __('Añade un tema') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <!-- modal body / form  -->
      <form method="POST" action="{{ route('temas.store') }}">
        {{ csrf_field() }}

        <input type="hidden" name="curso_id" value="{{ $curso_ -> id }}">
        <div class="modal-body text-left">
          <div class="form-group d-block mt-2">
            <label class="control-label" for="title">{{ __('Titulo') }}</label>
            <input type="text" class="form-control{{ $errors->add_tema->has('title') ? ' is-invalid' : ''}}" id="title" placeholder="Titulo del tema" name="title" value="{{ old('title') }}" />
            @if($errors->add_tema->has('title'))
                <small class="form-text invalid-feedback">{{ $errors->add_tema->first('title') }}</small>
            @endif
          </div>
          <div class="form-group mt-2">
            <textarea class="form-control{{ $errors->add_tema->has('descripcion') ? ' is-invalid' : ''}}" id="descripcion" rows="3" placeholder="{{ __('Breve descripcion sobre el tema (*)') }}" aria-describedby="descripcionHelp" name="descripcion"></textarea>
            <small id="descripcionHelp" class="form-text text-muted">{{ _('* Opcional.') }}</small>
            @if($errors->add_tema->has('descripcion'))
                <small class="form-text invalid-feedback">{{ $errors->add_tema->first('descripcion') }}</small>
            @endif
          </div>
        </div>
      <!-- modal body / form  -->
        <div class="modal-footer">
        <button type="submit" class="btn btn-danger">{{ __('Añadir') }}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cerrar') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')

  @if(count($errors->add_tema)>0)
  <script>
  $(document).ready(function(){ 
      $('#addTema').modal({show: true});
  });
  </script>
  @endif

@endpush