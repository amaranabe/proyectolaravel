<!-- Modal de aviso para publicar un curso -->
<div class="modal fade" id="publicarModal" tabindex="-1" role="dialog" aria-labelledby="publicarCurso" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="publicarCurso"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;&nbsp;Advertencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center m-3">
        Antes de proceder a visualizar el curso en la plataforma elearning, asegurese de que tiene todo el temario y sus recursos, listos para su visualización.
        <h6  class="mt-3 py-3 text-white bg-info">¿Está seguro de publicar su curso?</h6>
      </div>
      <div class="modal-footer">
        <form method="POST" action="{{ route('cursos.publicar', $curso->slug) }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <button type="submit" class="btn btn-warning">Publicar</button>
        </form>
      </div>
    </div>
  </div>
</div>