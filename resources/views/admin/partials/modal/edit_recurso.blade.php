<!-- Modal para crear Recurso -->
<div class="modal fade" id="editMedia" tabindex="-1" role="dialog" aria-labelledby="editMediaModal" aria-hidden="true">
  <div class="modal-dialog">
    <!-- modal content -->
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <h5 class="modal-title" id="editMediaModal">{{ __('Editar Recurso') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- modal body / form  -->
        <form id="editRecursoForm" action="javascript:;" enctype="multipart/form-data" method="POST" accept-charset="UTF-8">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="modal-body text-lef">Titulo
        <input type="hidden" id="recurso_id" name="recurso_id" value="">
          <div class="form-group">
              <input type="text" name="title" class="form-control" value="" />
              <small class="form-text invalid-feedback errorTitle"></small>
          </div>
          <div class="form-group">
            <div class="control-label">Fichero</div>
            <div class="custom-file mt-2">
            <input 
              type="file" 
              class="custom-file-input" 
              id="myfile"
              name="myfile" 
              value=""
              lang="es" 
              autoComplete="off" />
            <label class="custom-file-label" for="myfile"><small></small></label>
                <small class="form-text invalid-feedback errorFile"></small>
            </div>
          </div>      

        </div>
      <!-- modal body / form  -->
        <div class="modal-footer">
        <button type="submit" class="btn btn-danger">{{ __('Subir') }}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cerrar') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>


@push('scripts')

  <script type="text/javascript">
  $(document).ready(function(){
      
      //Modal Submit vía Ajax
      $("#editRecursoForm").on('submit', function(e){
            //disable the default form submission
            e.preventDefault();
            var id = $("input[name='recurso_id']").val();
            var data = new FormData($(this)[0]);
            //var url = form.attr("action");
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "{{ route('recursos.update', 'id') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response){
                    var data = JSON.parse(response);
                    if(data.errors) {
                        if(data.errors.title) {
                          $("input[name='title']").addClass('is-invalid');
                          $(".errorTitle").text(data.errors.title);
                        }

                        if(data.errors.myfile) {
                          $("input[name='myfile']").addClass('is-invalid');
                          $(".errorFile").text(data.errors.myfile);
                        }
                        
                    } else {
                       showRecursos(response);
                       $("#editMedia").modal('hide');
                    }
                },
                error: function(){
                    alert("Ha habido un error. Vuelva a intentarlo.");
                    $("#editMedia").modal('hide');
                }
            });

      });
      
  });

  </script>
@endpush