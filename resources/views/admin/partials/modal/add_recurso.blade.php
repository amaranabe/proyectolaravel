<!-- Modal para crear Recurso -->
<div class="modal fade" id="addMedia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog">
    <!-- modal content -->
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <h5 class="modal-title" >{{ __('Inserte un recurso') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- modal body / form  -->
        <form id="addRecursoForm" action="javascript:;" enctype="multipart/form-data" method="POST" accept-charset="UTF-8">
        {{ csrf_field() }}

        <div class="modal-body text-lef">Titulo
        <input type="hidden" id="tema_id" name="tema_id" value="">
          <div class="form-group">
              <input type="text" name="title" class="form-control" placeholder="Titulo del fichero a mostrar" />
                <small class="form-text invalid-feedback errorTitle"></small>
          </div>
          <div class="form-group">
            <div class="control-label">Fichero</div>
            <div class="custom-file mt-2">
            <input 
              type="file" 
              class="custom-file-input" 
              id="myfile"
              name="myfile" 
              lang="es" />
            <label class="custom-file-label" for="myfile">{{ __('Selecciona fichero') }}</label>
                <small class="form-text invalid-feedback errorFile"></small>
            </div>
          </div>      

        </div>
      <!-- modal body / form  -->
        <div class="modal-footer">
        <button type="submit" class="btn btn-danger">{{ __('Subir') }}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cerrar') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>

@push('scripts')

  @if(count($errors->add_recurso)>0)
  <script>
  $(document).ready(function(){ 
      $('#addMedia').modal({show: true});
  });
  </script>
  @endif

  <script type="text/javascript">
  $(document).ready(function(){
      
      //Modal Submit vía Ajax
      $("#addRecursoForm").on('submit', function(e){
            //disable the default form submission
            e.preventDefault();
            var data = new FormData($(this)[0]);
            //var url = form.attr("action");
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "{{ route('recursos.store') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                success: function(response){
                    var data = JSON.parse(response);
                    if(data.errors) {
                        if(data.errors.title) {
                          $("input[name='title']").addClass('is-invalid');
                          $(".errorTitle").text(data.errors.title);
                        }

                        if(data.errors.myfile) {
                          $("input[name='myfile']").addClass('is-invalid');
                          $(".errorFile").text(data.errors.myfile);
                        }
                        
                    } else {
                       showRecursos(response);
                       $("#addMedia").modal('hide');
                    }
                },
                error: function(){
                    alert("Ha habido un error. Vuelva a intentarlo.");
                }
            });
            //$("#addMedia").modal('hide');
      });
      
  });

  </script>
@endpush



