<!-- Tabla que se muestra en la vista admin.compras --> 
<div class="table-responsive-md">
	<table class="table mt-2">
	  	<thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Titulo</th>
		      <th scope="col" >Precio</th>
		      <th scope="col" >Desc</th>
		      <th scope="col" >Total €/curso</th>
		      <th scope="col" >Alumnos</th>
		      <th scope="col" >Total</th>
		      
		   </tr>
		</thead>
		<tbody>
		  	@php $ingresos = 0 @endphp
		    @foreach($compras as $detalle)
			    <tr>	
			      <th class="text-center" scope="row">{{ $detalle -> curso_id }}</th>
			      <td>{{ $detalle -> curso->titulo}}</td>
			      <td>{{ $detalle -> precio}} €</td>
			      <td>{{ $detalle -> descuento}}%</td>
			      <td>{{ $detalle -> getTotal() }} €</td>
			      <td>{{ $detalle->curso->users_count }}</td>
			      <td>{{ $detalle -> getTotal()* $detalle->curso->users_count}} €</td>
			    </tr>
			    @php 
			    	$total = $detalle -> getTotal()* $detalle->curso->users_count;
			    	$ingresos = $ingresos + $total;
			    @endphp
		    @endforeach
		    	<tr class="border-bottom-1">
		    		<td colspan="6" class="text-right"><h4>Ingresos total:</h4></td>
		    		<td><h5>{{ $ingresos }} €</h5></td>
		    	</tr>
		</tbody>
	</table>
	<div class="mt-2 d-flex justify-content-lg-center">	{{ $compras -> links () }} </div>
</div>