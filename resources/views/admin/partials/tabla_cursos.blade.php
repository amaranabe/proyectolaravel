<!-- tabla que lista todos los cursos -->
<div class="table-responsive">
<table class="table mt-2">
  	<thead>
	    <tr>
	      <th class="text-center" scope="col">#</th>
	      <th scope="col">Titulo</th>
	      <th class="text-right" scope="col" >Precio</th>
	      <th class="text-right" scope="col" >Estado</th>
	      <th class="text-right" scope="col" >Opciones</th>
	    </tr>
		</thead>
  	<tbody>
    @foreach($cursos as $curso)
	    <tr>	
	      <th class="text-center" scope="row">{{ $curso -> id }}</th>
	      <td>{{ $curso -> titulo}}</td>
	      <td class="text-right">{{ $curso -> precio}} €</td>
	      <td class="text-right">
	      		@switch($curso -> estado)
				    @case(1)
				        {{ __('PENDIENTE') }}
				        @break

				    @case(2)
				        {{ __('PUBLICADO') }}
				        @break

				    @case(3)
				        {{ __('ELIMINADO') }}
				@endswitch
		 </td>
	      <td class="td-actions text-right">					
			<div class="d-inline-flex">
				<a href="{{ url('/admin/cursos/'. $curso -> slug) }}" rel="tooltip" title="Ver curso" class="btn btn-info btn-xs mr-1"><i class="fa fa-info-circle"></i></a href="">
	      		<a href="{{ url('/admin/cursos/'. $curso -> slug .'/editar') }}" rel="tooltip" title="Editar curso" class="btn btn-success btn-xs mr-1"><i class="fa fa-edit"></i></a>
	      	<form method="post" action="{{ url('/admin/cursos/'. $curso -> id) }}">{{ csrf_field()}}
	 			{{ method_field('DELETE')}}
	 			<button type="submit" rel="tooltip" title="Eliminar curso" class="btn btn-danger btn-xs mr-1"><i class="fa fa-times"></i></button>
	 		</form>
			</div>
	      </td>
	    </tr>
    @endforeach
  </tbody>
</table>
</div>
