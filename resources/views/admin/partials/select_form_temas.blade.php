
              <div class="card mt-3"> 
                  <div class="card-body">
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <label class="input-group-text">Tema</label>
                          </div>
                          <select class="custom-select" id="titulosTemas">
                            <option value="default">{{ __('-- Seleccione un tema --') }}</option>
                            @foreach($curso_->temas as $tema)
                                  <option id="tema{{$tema->id}}" value="{{ $tema->id }}">{{$tema->titulo}}</option>
                            @endforeach
                          </select>
                      </div>
                      
                  </div>
              </div>

