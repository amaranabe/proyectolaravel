<!-- Formulario de Curso -->


<form 
method="POST" 
action="{{ ! $curso_->id ? route('cursos.store') : route('cursos.update', $curso_->slug) }}" 
novalidate
enctype="multipart/form-data">
	{{ csrf_field() }}
	@if($method == 'PUT')
		{{ method_field('PUT') }}
	@endif

		<!-- titulo principal -->
		<div class="col-sm-12">
			<div class="form-group label-floating">
				<label for="titulo" class="control-label h5">Titulo principal</label>
				<input type="text" class="form-control form-control-lg{{ $errors->curso_form->has('titulo') ? ' is-invalid' : ''}}" name="titulo" value="{{ old('titulo') ?: $curso_->titulo}}"></input>
				@include('admin.partials.errors.form_alert', ['field' => 'titulo'])
			</div>
		</div>

		<!-- titulo secundario -->
		<div class="col-sm-12">
			<div class="form-group label-floating">
				<label for="titulo2" class="control-label">Titulo secundario <small>(opcional)</small></label>
				<input type="text" 
						class="form-control{{ $errors->curso_form->has('titulo2') ? ' is-invalid' : ''}}" 
						name="titulo2" 
						value="{{ old('titulo2') ?: $curso_->titulo2 }}"
						required
				></input>
				@include('admin.partials.errors.form_alert', ['field' => 'titulo2'])
			</div>
		</div>
		
		<!-- categoría-->
		<div class="col-sm-12">
			<div class="form-group label-floating">
				<label for="categoria_id" class="control-label">Categoría</label>
				<select class="form-control" name="categoria_id">
					@foreach(\App\Categoria::pluck('nombre', 'id') as $id => $nombre)
						<option {{ (int) old('categoria_id') === $id || $curso_->categoria_id === $id ? 'selected' : ''}} value="{{ $id }}">
							{{ $nombre }}
						</option>
					@endforeach
				</select>
			</div>
		</div>
			
		<!-- duración, precio y descuento -->
		<div class="col-sm-12 px-0 d-inline-flex">
			<div class="col-sm-4 form-group label-floating">
				<label class="control-label">Duración</label>
				<input type="number" class="form-control{{ $errors->curso_form->has('duracion') ? ' is-invalid' : ''}}" name="duracion" value="{{ old('duracion') ?: $curso_->duracion }}"></input>
				@include('admin.partials.errors.form_alert', ['field' => 'duracion'])
			</div>

			<div class="col-sm-4 form-group">
				<label class="control-label">Precio</label>
				<input type="number" class="form-control{{ $errors->curso_form->has('precio') ? ' is-invalid' : ''}}" name="precio" value="{{ old('precio') ?: $curso_->precio }}"></input>
				@include('admin.partials.errors.form_alert', ['field' => 'precio'])
			</div>

			<div class="col-sm-4 form-group">
				<label class="control-label">Descuento (%)</label>
				<input type="number" class="form-control{{ $errors->curso_form->has('descuento') ? ' is-invalid' : ''}}" name="descuento" value="{{ old('descuento') ?: $curso_->descuento }}"></input>
				@include('admin.partials.errors.form_alert', ['field' => 'descuento'])
			</div>		
		</div>
		<!-- imagen -->
		<div class="col-sm-12">
			<div class="form-group">
				<div class="control-label">Imagen</div>
				<div class="custom-file mt-2">
					<input 
						type="file" 
						class="custom-file-input{{ $errors->curso_form->has('image') ? ' is-invalid' : ''}}" 
						id="image"
						name="image" 
						lang="es" />
					<label class="custom-file-label" for="image"><small>
						{{ ! $curso_->id ? __('Selecciona portada curso') : $curso_->imagen }}
					</small></label>
					@include('admin.partials.errors.form_alert', ['field' => 'image'])

				</div>
			</div>			
		</div>

		<!--dirigido a -->
		<div class="col-sm-12">
			<div class="form-group">
				<label for="dirigido_a" class="control-label">Dirigido a</label>
				<textarea 
					class="form-control{{ $errors->curso_form->has('dirigido_a') ? ' is-invalid' : ''}}" 
					name="dirigido_a"
					rows="4"
				>{{ old('dirigido_a') ?: $curso_->dirigido_a}}</textarea >
				@include('admin.partials.errors.form_alert', ['field' => 'dirigido_a'])
			</div>
		</div>

		<!-- campos objetivos-->
		<div class="col-sm-12 mt-4">
			<!-- incluimos la vista blade, y le pasamos los objetivos por parámetros -->
			@include('admin.partials.objetivo_form')
		</div>


	@if(!empty($btnTexto))
	<div class="col-sm-12 mt-4 text-center">
		<button type="submit" class="mx-2 btn btn-outline-primary">{{ __($btnTexto) }}</button>

		<a href="{{ url('/admin') }}" class="mx-2 btn btn-secondary">Cancelar</a>
	</div>
	@endif

</form>


@push('scripts')

<script type="text/javascript">
	
$( document ).ready(function() {	
	//Cargar nombre del fichero en el input file
		$('.custom-file-input').on('change', function() { 
			
		    var fileName = $(this).val().split('\\').pop(); 
		    //alert(fileName);
		    $(this).next('.custom-file-label').addClass("selected").html(fileName); 
		});
});

</script>

@endpush