<div class="card">
  <div class="card-header">
    {{ __('Objetivos') }}
  </div>
  <div class="card-body">
    
    <div class="card-text">
    	<!-- Vamos a permitir que como máximo se introduzcan 2 objetivos, el primero requeido y el segundo opcional-->
		<div class="form-group label-floating">
			<label for="objetivo1" class="control-label">Primer objetivo</label>
			<textarea
					class="form-control{{ $errors->curso_form->has('objetivos.0') ? ' is-invalid' : ''}}" 
					name="objetivos[]" 
					required
					rows="4"
			>{{ old('objetivos.0') ? old('objetivos.0') : ($curso_->objetivos_count>0 ? $curso_->objetivos[0]->objetivo : '') }}</textarea>
			@include('admin.partials.errors.form_alert', ['field' => 'objetivos.0'])

			<!-- para tener un control sobre el input primer objetivo-->
			@if($curso_->objetivos_count > 0)
				<input type="hidden" name="objetivo_id0" value="{{ $curso_->objetivos[0]->id }}">
			@endif
		</div>

		<div class="form-group label-floating">
			<label for="objetivo2" class="control-label">Segundo objetivo <small>(opcional)</small></label>
			<textarea
					class="form-control{{ $errors->curso_form->has('objetivos.1') ? ' is-invalid' : ''}}" 
					name="objetivos[]" 
					rows="2"
			>{{ old('objetivos.1') ? old('objetivos.1') : ($curso_->objetivos_count>1 ? $curso_->objetivos[1]->objetivo : '') }}</textarea>
			@include('admin.partials.errors.form_alert', ['field' => 'objetivos.1'])

			<!-- para tener un control sobre el input del segundo objetivo-->
			@if($curso_->objetivos_count > 1)
				<input type="hidden" name="objetivo_id1" value="{{ $curso_->objetivos[1]->id }}">
			@endif
		</div>
    </div>

  </div>
</div>