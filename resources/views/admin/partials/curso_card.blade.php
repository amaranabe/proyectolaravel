<a href="{{url('/admin/cursos/'. $curso -> slug)}}"
   style="color: inherit; text-decoration:none;">
      <div class="card mb-4">
          <div class="card-body p-0">
            <div class="row">
            <div class="col-md-6">
                    <img src="{{ url('/storage/' . $curso -> imagen) }}" 
                    alt="{{ $curso->titulo }}" height="100%" class="card-img-top mr-3">
            </div>
            <div class="col-md-6 pl-0">
                <div class="info-top">
                    <h5 class="mt-2 d-block text-truncate" style="max-width: 375px;">{{ $curso->titulo }}</h5>
                    <h6 class="mt-2 d-block text-truncate" style="max-width: 375px;">{{ $curso->titulo2 }}</h6>
                    <span class="p-1 badge-secondary">{{ $curso->categoria->nombre }}</span><br>
                    <small>{{ __('Estudiantes') }}: {{ $curso->users_count }}</small>
                  </div>
                  <div class="info-bottom d-flex pt-4">
                      <div class="info-center-left">
                        <div>{{ $curso->created_at->format('d/m/Y') }}</div>
                        @include('partials.cursos.rating')
                      </div>
                      <div class="info-center-right ml-auto p-4">
                            <span><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;{{ number_format($curso->duracion, 0) }} h
                            </span>
                            <span class="ml-1">| {{ number_format($curso->precio, 0) }} €</i>
                            </span>
                      </div>
                  </div>
            </div>
                        
          </div> 
          <div class="card-footer text-right">
              <div class="btn-group" role="group" aria-label="Basic example">
                  <a href="{{ url('/admin/cursos/'. $curso -> slug .'/editar') }}" class="btn btn-outline-success btn-xs mr-1"><i class="fa fa-edit"></i>&nbsp;&nbsp;Editar</a>
                  <form method="post" action="{{ url('/admin/cursos/'. $curso -> id) }}">{{ csrf_field()}}
                    {{ method_field('DELETE')}}
                    <button type="submit" rel="tooltip" title="Eliminar curso" class="btn btn-outline-danger btn-xs mr-1"><i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar</button>
                  </form>
                  @if(($curso->estado == \App\Curso::PENDIENTE))
                    <button class="btn btn-outline-warning" class="btn btn-primary" data-toggle="modal" data-target="#publicarModal"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;&nbsp;Publicar</button>
                    @include('admin.partials.modal.publicar_curso')
                  @endif
                </div>
          </div>
          </div>
      </div>
</a>  