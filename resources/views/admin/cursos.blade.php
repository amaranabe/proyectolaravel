@extends('admin.layouts.master')

@section('title', 'Panel Administración')
@section('breadcrumbs', Breadcrumbs::render('cursos'))
@section('contenido')
 <!-- Panel principal de información -->
    <div class="row">
        <div class="col-sm-12">
          <div class="card mt-5">
            <div class="card-header d-inline-flex">
                <h4>{{ __('Cursos registrados') }}</h4>
                <a class="btn btn-outline-success ml-auto" href="{{ route('cursos.crear') }}">
                	<i class="fa fa-plus"></i>&nbsp;&nbsp;{{ __('Crear Curso') }}</a>
            </div>
            <div class="card-body">
              	<div class="info_panel">
          					<div class="row">
                      <!-- Tabla cursos -->
          						<!-- Mostraremos los cursos en 2 grid. Para ello Blade, tiene el método chunk, que permite romper el array en arrays múltiples más pequeños  -->
                      @forelse($cursos->chunk(2) as $chunk)
                        @foreach($chunk as $curso)
                        <div class="col-md-6">
                            @include('admin.partials.curso_card')
                        </div>
                        @endforeach
                      @empty
                          <div class="alert alert-secondary p-5 text-center">
                              {{ __("No tienes ningún curso todavía") }}<br/>
                              <a class="btn btn-success mt-2" href="{{ route('cursos.crear') }}">
                                {{ __('Crear nuevo curso') }}
                              </a>
                          </div>   
                      @endforelse
          						
                      <div class="col-md-12 mt-3">
                          <div class="d-flex justify-content-lg-center pt-2">
                            {{ $cursos -> links () }} 
                          </div>	
                      </div>
          					</div>
				        </div>
            </div>
          </div><!-- / card content -->
        </div>
    </div>
@stop

