@extends('admin.layouts.master')

@section('title', 'Compras')
@section('breadcrumbs', Breadcrumbs::render('compras'))
@section('contenido')

    <!-- Panel principal de información -->
    <div class="row">
        <div class="col-sm-12">
          <div class="card mt-5">
            <div class="card-header d-inline-flex">
                <h4>{{ __('Ingresos por curso') }}</h4>
            </div>
            <div class="card-body">
                  <div class="info_panel">
                    @include('admin.partials.tabla_compras')
					        </div>
				    </div>
          </div>
        </div><!-- / card content -->
    </div>
@stop
