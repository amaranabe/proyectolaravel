<div class="alert alert-info alert-dismissible text-center">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>	
	</button>
	<h2><strong><i class="fa fa-info-circle"></i>{{ session()->get('mensaje') }}</strong></h2>
</div>