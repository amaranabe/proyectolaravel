    <div class="col-lg-10 offset-lg-2 p-0">
      <nav class="navbar navbar-expand-lg d-none d-lg-block">
          <ul class="nav">
            <li class="nav-item">
              <h4><a class="nav-link active" href="{{ route('admin') }}">Dashboard</a></h4>
            </li>
            <div class="ml-auto">@include('partials.navitem_user')</div>
        </ul>
      </nav>
      <!-- include cabecera Breadcrumb -->
      @yield('breadcrumbs')

      <!-- include body -->
      <div class="contenido col-12">
          @yield('contenido') 
      </div><!-- / Contenido-->
    </div> <!-- / sidecontent. col-md-9 -->