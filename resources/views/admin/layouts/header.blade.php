
<!-- Creamos contenido del HEADER para PANEL DE ADMINISTRACIÓN -->
<nav class="navbar navbar-expand-lg navbar-light bg-light d-lg-none">
      <div class="container-fluid">
          <a href="{{ url('/') }}" class="navbar-brand">{{ __('#MariaJoseGoñi')}}</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div id="navbarNav" class="collapse navbar-collapse">
            <ul class="navbar-nav">
              <!-- inclir despleglabe de usuario -->
              @include('partials.navitem_user')
            </ul>
          </div>
      </div>
</nav>


