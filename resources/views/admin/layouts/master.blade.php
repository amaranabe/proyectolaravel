<!--  Plantilla que va a servir de base nuestro Panel de administración --> 
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" lang="es">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title', '#MaríaJoséGoñi')</title>


    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>


    <!-- Styles -->
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/admin_style.css') }}">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/popper.min.js')}}" type="text/javascript"></script>
  </head>
  <body>

    <header>
        @include('admin.layouts.header')
    </header>      
            
    <div class="container-fluid">

      <div class="row content">

        @include('admin.layouts.sidenav')
        @include('admin.layouts.sidecontent')

      </div><!-- / row -->
      
    </div><!-- / container-fluid -->
 
    <!-- Bootstrap Core JS -->
    <script src="{{ asset('js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/myjs.js')}}" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    @stack('scripts')

  </body>
</html>