    <div class="sidenav col-lg-2 d-none d-lg-block">
        <nav>
            <div id="profile" class="text-center">
                <a href="{{ url('/') }}">
                    <img src="{{ url('storage/images/portada.png') }}" class="rounded-circle" alt="..." width="100px" height="100px">
                    <h5 class="mt-3">{{ __('María José Goñi') }}</h5>
                </a>
            </div>
            <div class="socialmedia ">
              <ul class="list-inline social">
                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>      
            <div id="menu" class="menu" >
              <ul class="list-group" id="list-tab" role="tablist">
                <li data-toggle="collapse" data-target="#service" class="collapsed">
                    <a href="#">
                        <i class="fa fa-leanpub" aria-hidden="true"></i>
                        &nbsp;&nbsp;{{ __('Cursos') }}
                    </a>
                </li>
                <ul class="sub-menu collapse" id="service">
                    <li><a href="{{ url('/admin/cursos') }}">{{ __('Cursos registrados') }}</a></li>
                    <li><a href="{{ url('/admin/cursos/crear') }}">{{ __('Crear Curso') }}</a></li>
                </ul>

                <li><a href="{{ route('compras') }}">
                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;&nbsp;{{ __('Compras') }}</a></li>
                <li><a href="{{ route('usuarios') }}">
                        <i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;{{ __('Usuarios') }}</a></li>
              </ul>
            </div>           
        </nav>
    </div>