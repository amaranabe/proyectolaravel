<!-- Desplegable de cualquier usuario -->
<li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ Auth::user()->name }} 
      </a>
      <ul class="dropdown-menu dropdown-menu-right" aria-labeledby="menu_sesion">
          <li class="nav-item">
              <a class="dropdown-item" href="{{ route('verperfil') }}">
              <i class="fa fa-user" aria-hidden="true"></i>
                &nbsp;Mis datos</a>
          </li>
          <li class="nav-item">
              <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Cerrar sesión</a>
              <form id="logout-form" 
                      action="{{ route('logout') }}" 
                      method="POST" 
                      style="display: block;">
                      {{ csrf_field() }}
              </form>
          </li>
      </ul>
</li>