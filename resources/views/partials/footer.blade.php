
<!-- Footer -->
    <footer class="text-center my-0 bg-light">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                    <a href="#top" title="to Top">
                        <span><i class="fa fa-chevron-up"></i></span>
                    </a>
            </div>
            <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-6"> 
                            <div class="socialmedia">
                              <ul class="list-inline social">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                              </ul>   
                            </div>
                        </div>
                        <div class="col-md-6 d-flex justify-content-center">
                            <p><small>@amaiamb - 2018</small></p>
                        </div>
                    </div>
            </div>
        </div>  
    </footer>