<div class="card h-100 mb-4">
	<a href="{{ route('detalle-curso', $curso -> slug) }}" style="color: inherit; text-decoration: none;">
		<img class="card-img-top" src="{{ asset('/storage/' . $curso -> imagen) }}" alt="{{ ($curso -> titulo) }}" height="240">
		<div class="card-body d-flex flex-column">
			<h5 class="card-title">{{ ($curso -> titulo) }}</h5>
			<div class="row justify-content-center py-3">
				{{-- añadir parcial para mostrar el rating --}}
				@include('partials.rating')
			</div>
			<h5>
				<span class="badge">
					{{ strtoupper($curso->categoria->nombre) }}
				</span></h5>
			<h4 class="card-text py-3 text-right pr-2">
				@if($curso->descuento > 0)
					<s class="text-muted">{{ number_format($curso -> precio, 0) }} €</s>
					<strong class="text-danger">{{ number_format($curso -> getTotal(), 0) }} €</strong>
				@else
					{{ number_format($curso -> precio, 0) }} €
				@endif
			</h4>
			
			@include('partials.cursos.action_button')    
	    </div>
	</a>
</div>