<div id="catalogo" class="container wrapper-border">
      
        @if(session('aviso'))
        <!-- Aviso de curso añadido a cesta -->
            <div class="row mb-4">
                <div class="col-lg-12">
                  <div class="mb-0 alert alert-success alert-dismissible fade show" role="alert">{{ session('aviso') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif

      <h2 class="text-center">CURSOS</h2>
      @php($ncpv=3)
    @php ($nrows=floor(count($cursos)/$ncpv))
    @php ($cursos_restantes=count($cursos)%$ncpv)
    <div class="mt-5">
      <div class="row justify-content-center">
          <div class="">
              <div id="carouselCursos" class="carousel" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                      <!-- Slide 1           -->
                      @for($i=0; $i < $nrows; $i++)
                      <div class="carousel-item {{ $i==0 ? ' active' : '' }}">
                          <div class="row">
                              @for($j=0; $j < $ncpv ; $j++)
                                  @php( $curso=$cursos[$i*$ncpv+$j])
                                  <div class="col-lg-4 col-md-6 d-flex">
                                    @include('partials.card')
                                  </div>
                              @endfor
                          </div>
                      </div>
                      @endfor
                      @if($cursos_restantes>0)
                        <div class="carousel-item">
                        <div class="row">
                          @php($z = count($cursos) - $cursos_restantes)
                            @for($j=$z; $j < count($cursos) ; $j++)
                                @php( $curso=$cursos[$j])
                                <div class="col-lg-4 col-md-6 d-flex">
                                    @include('partials.card')
                                  </div>
                            @endfor
                        </div>
                      </div>
                      @endif
                  </div>

                   <!-- Controles anterior y posterior -->
                   <a class="carousel-custom-control-prev " href="#carouselCursos" role="button" data-slide="prev">
                    <button type="button" class="btn btn-outline-secondary rounded-circle btn-lg btn-master"><i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </button>
                    <span class="sr-only">Anterior</span>
                   </a>
                   <a class="carousel-custom-control-next" href="#carouselCursos" role="button" data-slide="next">
                    <button type="button" class="btn btn-outline-secondary rounded-circle btn-lg btn-master"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                    <span class="sr-only">Siguiente</span>
                   </a>
              </div>
          </div>
      </div>
    </div><!-- ./ Fin del container -->
</div>
    