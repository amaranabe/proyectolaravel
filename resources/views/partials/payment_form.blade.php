<!-- https://bootsnipp.com/snippets/5Mkl8 -->
<div class="row">
	<aside class="col">
		<article class="card pasarela-pago">
		<div class="card-body">
			<p class="text-center"> 
				<img src="http://bootstrap-ecommerce.com/main/images/icons/pay-visa.png"> 
				<img src="http://bootstrap-ecommerce.com/main/images/icons/pay-mastercard.png"> 
			   <img src="http://bootstrap-ecommerce.com/main/images/icons/pay-american-ex.png">
			</p>

			<form action="{{ url('cesta/pago') }}" 
				class="require-validation" 
				data-cc-on-file="false" 
				data-stripe-publishable-key="pk_bQQaTxnaZlzv4FnnuZ28LFHccVSaj" 
				id="payment-form" 
				method="post">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="email">E-mail</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-at"></i></span>
						</div>
						<input type="mail" class="form-control" id="email" name="email" placeholder="ejemplo@ejemplo.com" value="{{ old('email') }}" required data-stripe="stripeEmail">
					</div> <!-- input-group.// -->
				</div> <!-- form-group.// -->

				<div class="form-group">
					<label for="fullname">Nombre completo</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
						</div>
						<input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nombre y Apellidos" value="{{ old('fullname') }}" required>
					</div> <!-- input-group.// -->
				</div> <!-- form-group.// -->
				
				<!-- Formulario de pago de Stripe -->
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
					    <label for="card-element">
					      {{ __("Tarjeta de crédito o débito") }}
					    </label>
					    <div id="card-element">
					      <!-- A Stripe Element will be inserted here. -->
					    </div>

					    <!-- Used to display Element errors. -->
					    <div id="card-errors" role="alert"></div>
					 </div>
					</div>
				</div>
				<button type="submit" class="btn btn-outline-secondary btn-master btn-block btn-lg">{{ __("Confirmar pago") }}
				</button>

			</form>
		</div> <!-- card-body.// -->
		</article> <!-- card.// -->
	</aside> <!-- col.// -->
</div> <!-- row.// -->

