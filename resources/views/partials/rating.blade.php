<div class="mb-0">

	<ul class="list-inline mb-0">
		<li class="list-inline-item"><i class="fa fa-star" style="color:{{ $curso->valoracion >=1 ? ' #69c7ad' : ''}}" aria-hidden="true"></i></li>
		<li class="list-inline-item"><i class="fa fa-star" style="color:{{ $curso->valoracion >=2 ? ' #69c7ad' : ''}}" aria-hidden="true"></i></li>
		<li class="list-inline-item"><i class="fa fa-star" style="color:{{ $curso->valoracion >=3 ? ' #69c7ad' : ''}}" aria-hidden="true"></i></li>
		<li class="list-inline-item"><i class="fa fa-star" style="color:{{ $curso->valoracion >=4 ? ' #69c7ad' : ''}}" aria-hidden="true"></i></li>
		<li class="list-inline-item"><i class="fa fa-star" style="color:{{ $curso->valoracion >=5 ? ' #69c7ad' : ''}}" aria-hidden="true"></i></li>
	</ul>

</div>