
<!-- Creamos contenido del HEADER-->
<header class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">		
			<!-- Etiqueta para contener el titulo o logo-->
			<div class="navbar-header">
				<a class="navbar-brand p-2" href="{{ url('/') }}">#MaríaJoséGoñi</a>
			</div>
          
			<!-- Agregamos icono para dispositivos móviles-->
			<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Menu de navegación">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Navbar => Menú de navegación -->
			<div id="navbarContent" class="collapse navbar-collapse">
				<!-- Parte central del Navbar -->
	            <div class="header_center nav navbar-nav mr-auto ml-auto">
					
	            </div>


				<!-- Parte derecha del Navbar -->
                <div class="header_right pr-3">
                	<!-- menú de navegación propiamente dicho-->
	                <ul class="nav navbar-nav navbar-right">

	                    <li class="nav-item">
	                        <a href="{{ url('/cursos') }}" class="nav-link">{{ __('CURSOS') }}</a>
	                    </li>

						@guest

							<li class="nav-item">
								<a id="login" href="{{ url('/login') }}" class="nav-link">{{ __('LOGIN') }}</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/register') }}" class="nav-link">{{ __('REGISTRO') }}</a>
							</li>
		                @else
		                	@if(auth()->user()->admin)
			                	<li class="nav-item">
									<a href="{{ url('/admin') }}" class="nav-link">{{ __('ADMINISTRACIÓN') }}</a>
								</li>
		                	@else           
		                		<li class="nav-item">
									<a href="{{ route('miscursos') }}" class="nav-link">{{ __('MIS CURSOS') }}</a>
								</li>
							@endif
		                    @include('partials.navitem_user')
		                @endauth

			            <!-- Formulario de BÚSQUEDA de cursos-->
						<form id="search" class="form-inline mx-3" method="get" action="{{ url('/busquedas') }}" >
							<div class="form-group">
								<div class="input-group input-group-style">
									<input type="search" class="form-control" placeholder="Buscar curso..." aria-label="Buscar" name="consulta" id="busqueda">
								    <span class="input-group-append">
								    	<button type="submit"  class="btn my-0">
								        <i class="fa fa-search"></i>
								    	</button>
									</span>
								</div>
							</div>
	                	</form>
						<!-- Acceso a la cesta  -->
            			<li class="nav-item">
	                        <a href="{{ route('cesta') }}" class="nav-link" id="menu_cesta">
	                        	<i class="fa fa-shopping-cart fa-lg" style="font-size:30px">
	                        	<span class="badge badge-light cesta-count">
	                        		{{ $conter }}
	                        	</span></i>
	                    	</a>
	                    </li>					
					</ul><!-- fin del menú de navegación-->
				</div>
			</div>
	</nav>
</header>

	

@section('scripts_busqueda')
		<script type="text/javascript">
			$(function () {
				//inicializar la fuente de información
					var cursos = new Bloodhound({
							datumTokenizer: Bloodhound.tokenizers.whitespace,
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							prefetch: '{{ url("/cursos/json") }}' 
							//indicamos una url que devuelva un archivo json con los titulos de los cursos
						});


					//inicializar typeadead sobre nuestro input de búsqueda
					$('#busqueda').typeahead({
						hint: true,
						highlight: true,
						minLength: 1
					}, {
						name: 'cursos',
						source: cursos
					});
			});
		</script>
@endsection
