@section('jumbotron')
<div class="row">
	<div class="col-md-12">
		<div class="jumbotron p-5">
		  <div class="container">
		    <h1 class="display-4">
				<i class="fa fa-{{ $icono }}"></i>
				{{ $titulo }}
		    </h1>
		    <p class="lead mb-0">{{ $query }}</p>
		  </div>
		</div>
	</div>
</div>
@stop