<div id="valoraciones" class="row wr_valoraciones_curso mt-5">
		<div class="col-md-10 offset-md-1">
			<!-- Titulo section -->
			<div class="border-bottom">
				<h3 class="text-muted">{{ __('Valoraciones') }}</h3>		
			</div>
			<!-- Cuerpo section -->
			<div class="my-4">
				@forelse($curso->ratings as $rating)
					<div class="card col-md-12 col-lg-8 offset-lg-2 listing-block">
						<div clas="card-body">
							<div class="card-title mt-2">
								<div class="d-flex" style="height: 25px;">
									<p>{{ $rating->user->name }}<small class="mx-3">|</small></p>
									@include('partials.cursos.rating')
								</div>
								<small class="my-0"><strong>{{ $rating->created_at->format('d/m/Y') }}</strong></small>
							</div>

							<p>{{ $rating->comentario }}</p>

						</div>
					</div>
				@empty
					<div class="alert alert-light">
						<i class="fa fa-info-circle"></i>&nbsp;
						{{ __('Sin valoraciones todavía') }}
					</div>
				@endforelse
			</div>
		</div>
	</div>