@auth
	@can('view', $curso)
		@if($from)
			<ul class="nav flex-column">
			@foreach($tema->recursos as $recurso)
				<li class="nav-item">
					<button class="btn btn-link show-file" onclick="showFile({{ $recurso->id }}, {{ $curso->id }}, '{{ $recurso->tipo }}', '{{ $recurso->extension }}', '{{ $recurso->mimetype }}')" data-tipo="{{ $recurso->tipo }}" data-id="{{ $recurso->id }}" value="{{ $curso->id }}">
				  		<i class="fa fa-play-circle" aria-hidden="true"></i>&nbsp; &nbsp;{{ $recurso->titulo }}</button >
				</li>
			@endforeach
			</ul>
		@else
			<ul class="nav flex-column">{{ $tema->descripcion }} </ul>
		@endif
	@else
		{{ $tema->descripcion }}
	@endcan
@else
	<ul class="nav flex-column">{{ $tema->descripcion }}</ul>
@endauth


@push('scripts')
<script type="text/javascript">

</script>
@endpush



