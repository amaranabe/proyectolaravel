@cannot('inscribir', $curso)
	@can('valorar', $curso)
		<div class="row">
			<div class="col-lg-12">
				<div class="border-bottom">
					<h3 class="text-muted">{{ __('Queremos saber lo que opinas') }}</h3>
				</div>
				<div class="container-fluid">
					<form method="POST" action="{{ url('/add_valoracion') }}" class="form-inline" id="rating_form">
						{{ csrf_field() }}	
						<div class="form-group mr-5">
							<ul id="list_rating" class="list-inline" style="font-size: 30px;">
								<li class="list-inline-item star" data-number="1"><i class="fa fa-star"></i></li>
								<li class="list-inline-item star" data-number="2"><i class="fa fa-star"></i></li>
								<li class="list-inline-item star" data-number="3"><i class="fa fa-star"></i></li>
								<li class="list-inline-item star" data-number="4"><i class="fa fa-star"></i></li>
								<li class="list-inline-item star" data-number="5"><i class="fa fa-star"></i></li>
							</ul>
						</div>

						<input type="hidden" name="rating_input" value="1"/>
						<input type="hidden" name="curso_id" value="{{ $curso->id }}"/>

						<div class="form-group">
							<div class="col-6">
								<textarea 
									placeholder="{{ __("Escribe una reseña") }}"
									id="msj_valoracion"
									name="msj_valoracion"
									class="form-control"
									rows="3"
									cols="50"></textarea>
							</div>
						</div>
						<button type="submit" class="btn btn-outline-secondary btn-master btn-lg ml-5">{{ __("Valorar curso") }}</button>
					</form>
				</div>
			</div>	
		</div>
		@section('scripts_in_views')
	<script type="text/javascript">
		$(document).ready(function () {
			var rating = $('#list_rating');
			rating.find('li').on('click', function() {
				var numstar = $(this).data('number');
				$("#rating_form").find('input[name=rating_input]').val(numstar);
				rating.find('li i').each(function(index) {
					if((index +1) <= numstar) {
						$(this).css("color", "#69c7ad");
					}
					else {
						$(this).css("color", "grey");
					}
				});

			});
		});
	</script>
@stop
	@endcan
@endcannot