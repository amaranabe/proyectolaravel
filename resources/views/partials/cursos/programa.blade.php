<!-- Collaspse con el temario del curso-->
<div id="programa" class="row wr_programa_curso">
	<div class="col-lg-12 my-3">
		<div class="border-bottom">
			<h3 class="text-muted">Programa</h3>
		</div>

		<div class="accordion mt-3" id="accordionTemas" 
			role="tablist" 
			aria-multiselectable="true">
			@foreach($curso->temas as $key => $tema)
			    <div class="card">
				    <div class="card-header" role="tab" id="heading{{ $key }}">
				        <a class="accordion-toggle collapsed" role="button" data-toggle="collapse" 
				        href="#collapse{{ $key }}" aria-expanded="false" aria-controls="collapse{{ $key }}">
				        	<h6 class="card-title mb-0">Tema&nbsp;{{ $tema->posicion }} .&nbsp; {{ $tema -> titulo }}
				            <i class="float-right fa fa-plus-circle"></i></h6>
				        </a>
				    </div>
				    <div id="collapse{{ $key }}" class="collapse" role="tabcard" aria-labelledby="heading{{ $key }}" data-parent="#accordionTemas">
					        <div class="card-body">
					          @include('partials.cursos.action_temas')
					        </div>
				    </div>
				</div>
			@endforeach
        
  		</div>
	</div>
</div>

