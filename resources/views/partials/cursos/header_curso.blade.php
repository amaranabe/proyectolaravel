@if(session('aviso'))
	<div class="wr-info">
		<div class="mb-0 alert alert-success alert-dismissible fade show" role="alert">{{ session('aviso') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
		</div>
	</div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- Cabecera del curso -->
		<div class="jumbotron">
		  	<div class="container">
		    	<div class="row">
		    		<div class="col-md-8">
			    		<h1>{{ $curso->titulo }}</h1>
			    		<h3>{{ $curso->titulo2 }}</h3>
			    		<h4><span class="badge badge-secondary">{{ $curso->categoria->nombre }}</span></h4>
			    		<h5>{{ __("Actualizado") }}: {{ $curso->created_at->format('d/m/Y') }}</h5>
			    		<h6>{{ __("Estudiantes") }}: {{ $curso->users_count }}</h6>
			    		<h6>{{ __("Valoraciones") }}: {{ $curso->ratings_count }}</h6>
			    		@include('partials.cursos.rating')
			    	</div>	
			    	<div class="col-md-4 align-items-center my-auto">
			    		@include('partials.cursos.action_button')
			    	</div>
		    	</div>
		  	</div>
		</div>
	</div>

</div>	