{{-- Hago uso de la política creada en CursoPolicy, 
  -- que permite acceso al contenido de ese curso, si estoy registrado y entre 
  -- los usuarios que han comprado.  
  -- En caso contrario, permite o añadir cesta o ver cesta.
  -- Y si es usuario admin, permite acceso sólo al contenido del curso.
  --}}
@auth
	@if(!auth()->user()->admin)
		@can('inscribir', $curso)
			@can('addCursoCesta', $curso)
				<form method="post" action="{{ url('/cesta') }}" id="add_form">
					{{ csrf_field() }}
				    <input type="hidden" name="curso_id" value="{{ $curso-> id }}">
				    <input type="hidden" name="precio" value="{{ $curso-> precio }}">
				    <input type="hidden" name="descuento" value="{{ $curso-> descuento }}">
				    <button type="submit" id="add-curso" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto"><i class="fa fa-cart-plus"></i>{{ __(" Añadir a la cesta") }}</button>
				</form>
			@else
				 <a href="{{ url('/cesta') }}" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto"><i class="fa fa-shopping-cart"></i>{{ __(" Ver cesta") }}</a>
			@endcan
		@else
			<a href="{{ route('cursar', $curso -> slug) }}" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto">
				<i class="fa fa-unlock"></i>&nbsp;{{ __("Cursar") }}
			</a>
		@endcan
	@else
			<a href="{{ route('cursar', $curso -> slug) }}" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto">
				<i class="fa fa-unlock"></i>&nbsp;{{ __("Visualiza contenido") }}
			</a>
	@endif
@else
	@if( session('cesta_anonima') && (array_search($curso-> id, array_column(session('cesta_anonima'),'curso_id'))) !== false)
			 <a href="{{ url('/cesta') }}" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto"><i class="fa fa-shopping-cart"></i>{{ __(" Ver cesta") }}</a>
	@else
		<form method="post" action="{{ url('/cesta') }}" id="add_form">
				{{ csrf_field() }}
			    <input type="hidden" name="curso_id" value="{{ $curso-> id }}">
			    <input type="hidden" name="precio" value="{{ $curso-> precio }}">
			    <input type="hidden" name="descuento" value="{{ $curso-> descuento }}">
			    <button type="submit" id="add-curso" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-auto"><i class="fa fa-cart-plus"></i>{{ __(" Añadir a la cesta") }}</button>
			</form>
	@endif
@endauth