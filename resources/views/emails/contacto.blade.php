@component('mail::message')
## {{ $data['nombre'] }} te envía el siguiente mensaje:
<hr>
<br>
Asunto: {{ $data['asunto'] }}
@component('mail::panel')
	<p class="card-text">{{ $data['cuerpoDelMensaje'] }}</p>
@endcomponent

Enviado por [{{ $data['email'] }}](mailto:{{ $data['email'] }})

@endcomponent
