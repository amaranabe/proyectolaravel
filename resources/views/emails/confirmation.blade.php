@component('mail::message')
## Mensaje de bienvenida

Hola {{ $user->name }}, bienvenido a {{ env('APP_NAME') }}!!

Gracias por registrarse. Espero que esta plataforma elearning sea de su agrado.

Para continuar con el proceso de registro debe confirmar su cuenta de correo electrónico. Para ello, simplemente haga click en el siguiente enlace:

@component('mail::button', ['url' => config('app.url').'/users/confirmation/' . $user->confirmation_code])
Click para confirmar tu email
@endcomponent

Y si llega a olvidar su contraseña, la podrá recuperar a través de este correo.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent
