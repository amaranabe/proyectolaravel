<!--  Plantilla que va a servir de base para todas nuestras páginas. 
Crearemos una estructura común que tendrán el resto de páginas --> 
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, mininum-scale=1 user-scalable=no">
    <meta name="author" content="amaiamb">
    <meta name="description" content="@yield('description', 'Aplicación web que ofrece cursos online para apoyo a personas con discapacidad intelectual')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title', '#MaríaJoséGoñi')</title>


    <!-- Icons Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>


    <!-- Styles -->
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/popper.min.js')}}" type="text/javascript"></script>
  </head>
  <body id="top" data-spy="scroll" data-target=".navbar" data-offset="60">
            
        @include("partials.header")
        @yield('jumbotron')

        @yield('contenido')

        @include("partials.footer")
   
    <!-- Bootstrap Core JS -->
    <script src="{{ asset('js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/myjs.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/typeahead.bundle.min.js')}}" type="text/javascript"></script>
    <!-- Para aprovechar al máximo la funcionalidad avanzada de fraude de Stripe, incluya esta secuencia de comandos en cada página de su sitio, no solo en la página de pago. Incluir el script en cada página permite a Stripe detectar un comportamiento anómalo que puede ser indicativo de fraude a medida que los usuarios navegan por su sitio web. -->
    <script src="https://js.stripe.com/v3/"></script>
    @yield('scripts_busqueda')
    @yield('scripts_in_views')
    @stack('scripts')
  </body>
</html>