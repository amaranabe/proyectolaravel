<!--  Plantilla que va a servir de base para todas nuestras páginas. 
Crearemos una estructura común que tendrán el resto de páginas --> 
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, mininum-scale=1 user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title', '#MaríaJoséGoñi')</title>


    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>


    <!-- Styles -->
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/popper.min.js')}}" type="text/javascript"></script>

    <style type="text/css">
        html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: -webkit-box;
  display: flex;
  -ms-flex-align: center;
  -ms-flex-pack: center;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.card {
  border-radius: 0;
}

    </style>
  </head>
  <body>
    <div id="app" class="container-fluid">
        @if(session('mensaje'))
            <div class="row justify-content-center">
                <div class="col-md-10 alert alert-danger">
                    <p>{{ session('mensaje') }}</p>
                </div>
            </div>
        @endif

        @yield('contenido')
    </div>
  
    
    <!-- Bootstrap Core JS -->
    <script src="{{ asset('js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/myjs.js')}}" type="text/javascript"></script>
  </body>
</html>