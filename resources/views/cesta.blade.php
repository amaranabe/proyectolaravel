@extends('layouts.master')
@section('description', 'Cesta de la compra de cursos que ofertan recursos para apoyo a personas con discapacidad intelectual')
@section('title', 'Cesta compra #MariaJoseGoñi')
@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __("Cesta"),
        "icono" => "shopping-cart",
        "query" => ""
    ])
    
@stop
@section('contenido')
<main class="container wrapper-border">
	<div class="row mt-3">
		<div class="col">
			<div class="info_panel">
			@if(count($detallesCesta))
				<!-- Mensaje de aviso -->
				@if(session('aviso'))
					<div class="row mb-4">
						<div class="col-md-12">
							<div class="alert alert-danger">
								{{ session('aviso') }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                  <span aria-hidden="true">&times;</span>
				                </button>
							</div>
						</div>
					</div>
				@endif
				<div class="row">
					<div class="container border-bottom">
					    <h5>Tiene <strong>{{ count($detallesCesta) }}</strong> cursos en la cesta</h5>
					</div>
					<div class="table-responsive  col-md-8">
						<table class="table px-0 my-5">
					  	<thead>
						    <tr>
						      <th class="" scope="col">Curso</th>
						      <th class="" text-right" scope="col" >Precio</th>
						      <th class="" text-right" scope="col" >Dto.</th>
						      <th class="" text-right" scope="col" >Total</th>
						      <th class="" text-right" scope="col" >Opciones</th>
						    </tr>
				  		</thead>
					  	<tbody>
							@foreach($detallesCesta as $indice => $detalle)
							<tr>

								<td class="text-truncate"><a class="btn btn-link" href="{{ url('/curso/' . $detalle-> curso-> slug) }}">{{ $detalle-> curso -> titulo}}</a></td>
								<td class="" text-right" >{{ $detalle-> curso -> precio}} €</td>
								<td class="" text-right" >{{ $detalle-> curso -> descuento}} %</td>
								<td class="" text-right" >{{ number_format($detalle-> getTotal(), 2, '.', ',')}} €</td>
								<td class="" td-actions text-right">					
									<form method="post" action="{{ url('/cesta') }}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<input type="hidden" name="detalle_cesta_id" value="{{ $detalle -> id }}">
										<input type="hidden" name="detalle_cesta" value="{{ $indice }}">
										<button type="submit" rel="tooltip" title="Eliminar curso" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
									</form>
								</td>
							</tr>
							@endforeach
					  	</tbody>
					</table>
					</div>
					<div class="col-lg-2 offset-lg-1 mt-5 mb-3 text-center ">
						<p>Total:</p>
						<h2>{{ $total }} €</h2>
						<a href="{{ url('cesta/checkout') }}" class="btn btn-outline-secondary btn-master btn-lg mt-3">
							Comprar ahora
						</a>
						<a href="{{ route('cesta-trash') }}" class="mt-3">Vaciar cesta<i class="fa fa-trash ml-1"></i></a>
					</div>
					<div class="col text-center my-3">	
						<hr class="mb-5">
							<a href="{{ url('/catalogo') }}" class="btn btn-outline-secondary btn-master"><i class="fa fa-chevron-circle-left  mr-2"></i>Seguir comprando</a>
					 </div>						 
				</div>
			@else
				@if(session('alerta'))
						<div class="alert alert-success">
							{{ session('alerta') }}
						</div>
				@endif
				<h3 class="mt-5 text-center"><span class="label label-warning">Tu cesta está vacía.</span></h3>
			@endif
			</div>
		</div>
	</div>
</main>

@stop