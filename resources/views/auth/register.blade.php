@extends('layouts.template')

@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        @if(session('message'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('message') }}
            </div>
        @endif
        <div class="col-md-8 offset-md-2 m-auto">
            <div class="card">
                <div class="card-header card-header-template">
                    <h3 class="text-center"><a href="{{ url('/') }}" class="h5 mt-1 mb-0 pull-right" aria-label="Close" onclick="window.close()"><span aria-hidden="true"><i class="fa fa-times"></i></span></a>
                    {{ __("Regístrese") }}</h3> 
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3 mt-3">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nombre de usuario" required>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <input id="apellido1" type="text" class="form-control{{ $errors->has('apellido1') ? ' is-invalid' : '' }}" name="apellido1" value="{{ old('apellido1') }}" placeholder="Primer Apellido">

                                @if ($errors->has('apellido1'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('apellido1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <input id="apellido2" type="text" class="form-control{{ $errors->has('apellido2') ? ' is-invalid' : '' }}" name="apellido2" value="{{ old('apellido2') }}" placeholder="Segundo Apellido">

                                @if ($errors->has('apellido2'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('apellido2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña" required>
                            </div>
                        </div>


                        @if(config('settings.reCaptchStatus'))
                            <div class="form-group">
                                <div class="col-md-8 offset-md-2 mt-3">
                                    <div class="g-recaptcha" data-sitekey="{{ config('settings.reCaptchSite') }}"></div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group mt-5">
                            <div class="col-md-8 offset-md-2 mt-3">
                                <button type="submit" class="btn btn-outline-secondary btn-master btn-lg btn-block">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

