@extends('layouts.template')

@section('contenido')
<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col-md-6 offset-md-3 m-auto">
            @if(session('notificacion'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>{{ session('notificacion') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header card-header-template">
                    <h3><a href="{{ url('/') }}" class="h5 mt-1 mb-0 pull-right" aria-label="Close" onclick="window.close()"><span aria-hidden="true"><i class="fa fa-times"></i></span></a>
                    </a>{{ __("Inicie sesión") }}</h3>                    
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col mt-3">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuérdame
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col text-center">
                                <span class="border-top"></span>
                                <button type="submit" class="btn btn-outline-secondary btn-master btn-block btn-lg mt-2">
                                    Login
                                </button>
                        </div>
                        <div class="form-group mt-4">
                            <a href="{{ route('password.request') }}">
                                    {{ __('¿Has olvidado tu contraseña?') }}
                                </a>
                        </div>
                        <div class="form-group mt-4">                     
                            <a href="{{ route('register') }}" class="">
                                    {{ __('¿Aún no tienes cuenta? ') }}<strong>{{ __('Regístrate aquí') }}</strong>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
