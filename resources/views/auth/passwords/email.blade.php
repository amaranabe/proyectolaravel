
@extends('layouts.template')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2 m-auto">
            <div class="card">
                <div class="card-header card-header-template">
                    <h5 class="text-center">
                        <a href="{{ url('/') }}" class="pull-right" aria-label="Close" onclick="window.close()"><span aria-hidden="true"><i class="fa fa-times"></i></span></a>
                        {{ __('Restablecer contraseña') }}
                    </h5>                
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>{{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} text-center" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row my-4">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-outline-secondary btn-master btn-block btn-lg">
                                    {{ __('Cambiar contraseña') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
