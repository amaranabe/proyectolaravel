@extends('layouts.master')

@section('contenido')
  <!-- Container Jumbotron index-page -->
        
          <!-- Aviso de cuenta dada de baja -->
          @if(session('header_message'))
            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                {{ session('header_message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
          @endif




        <!-- Contenedor (Section PORTADA Jumbotron) -->
        <div class="container">
            <div class="jumbotron">
                  <div class="row d-flex align-items-center">
                    <div class="col-md-3 offset-md-1 d-none d-md-block">
                        <img src="{{ asset('/storage/images/logo.jpg') }}" width="175" alt="#MaríaJoséGoñi">
                    </div>
                    <div class="col-md-8">
                      <h1 class="display-4 text-center">Continúa aprendiendo</h1> 
                      <h5 class="pt-3 text-center">Formación especializada en la atención a personas con discapacidad intelectual</h5>
                    </div>
      
                  </div>
            </div>
        </div>
        
  <div class="col-12 p-0">
      <!-- menú secundario -->
      <div class="navbar secondary-navbar d-none d-md-block">
          <div class="mx-auto pt-3 text-center">
            <a href="#catalogo" class="btn btn-link">Cursos</a>
            <a href="#servicio" class="btn btn-link">Servicios</a>
            <a href="#about" class="btn btn-link">Conóceme</a>
            <a href="#contacto" class="btn btn-link">Contacto</a>
          </div>
      </div>

        <!-- Contenedor (Section CATÁLOGO) -->
      <div class="d-none d-md-block">
        @include('partials.slide')
      </div>
      <div class="row text-center">
        @if(session('aviso'))
          <div class="wr-info">
            <div class="mb-0 alert alert-success alert-dismissible fade show" role="alert">{{ session('aviso') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
          </div>
        @endif
        <div class="container wrapper-border d-block d-md-none">
            <h2 class="">CURSOS</h2>
            <div class="mt-5"> 
                @forelse($cursos as $curso)
                      <div class="col-md-4">
                          @include('partials.card')
                      </div>
                  @empty
                      <div class="alert alert-danger">
                          {{ __("No hay ningún curso disponible") }}
                      </div>
                  @endforelse
            </div>
        </div>
      </div>
      
  </div>




  <!-- Contenedor (Section SERVICIO APOYO) -->
  <div id="servicio" class="container text-center wrapper-border">
        <div class="row">
            <div class="col-sm-12  mb-3">
                <h2>SERVICIOS DE APOYO</h2>
                <h4>Ofrezco...</h4>
            </div>
        </div>
        <div class="row slideanim">
                <div class="col-md-4">
                    <span class="p-4">
                      <i class="fa fa-graduation-cap fa-5x" aria-hidden="true"></i>
                    </span>
                    <h4>EXPERIENCIA</h4>
                    <p>Lorem ipsum dolor sit amet..</p>
                </div>
                <div class="col-md-4">
                    <span class="p-4">
                      <i class="fa fa-pencil-square-o fa-5x" aria-hidden="true"></i>
                    </span>
                    <h4>PERSONALIZADO</h4>
                    <p>Lorem ipsum dolor sit amet..</p>
                </div>
                <div class="col-md-4">
                    <span class="p-4">
                      <i class="fa fa-users fa-5x" aria-hidden="true"></i>
                    </span>
                    <h4>CENTRADO EN LA PERSONA</h4>
                    <p>Lorem ipsum dolor sit amet..</p>
                </div>
            </div>
  </div>


<!-- Contenedor (Section ABOUT) -->
      <div id="about" class="container wrapper-border">
        <div class="row justify-content-center align-items-center">
          <img src="{{ asset('/storage/images/portada.png') }}" class="rounded-circle" alt="#MaríaJoséGoñi" width="200" height="200">
         
          <div class="col-sm-12 pb-3">
            <!-- INSERTAR FOTO -->
            <h2 class="text-center pt-4">CONÓCEME</h2>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4>
          </div>
          <div class="col-sm-12">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              
          </div>
        </div>
      </div>



  <!-- Contenedor (Section CONTACTO) -->
  <div id="contacto" class="container wrapper-border">
    <h2 class="text-center">CONTACTA CONMIGO</h2>
    <div class="row justify-content-center "> 
      <div class="col-sm-8 slideanim">
        @if(session()->has('aviso_email'))
            <div class="alert alert-success">{{ session()->get('aviso_email') }}</div>
        @endif
          <form method="post" action="{{ url('contacto') }}">
            {{ csrf_field() }}
              <div class="row">
                  <div class="col-sm-6 form-group">
                      <input class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}" type="text" required>
                      @if($errors->has('nombre'))
                          <small class="form-text invalid-feedback">{{ $errors->first('nombre') }}</small>
                      @endif
                  </div>
                  <div class="col-sm-6 form-group">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" placeholder="Email" value="{{ old('email') }}" type="email" required>
                      @if($errors->has('email'))
                          <small class="form-text invalid-feedback">{{ $errors->first('email') }}</small>
                      @endif
                  </div>
              </div>
              <div class="form-group">
                      <input class="form-control{{ $errors->has('asunto') ? ' is-invalid' : '' }}" id="asunto" name="asunto" placeholder="Asunto" value="{{ old('asunto') }}" type="text" required>
                      @if($errors->has('asunto'))
                          <small class="form-text invalid-feedback">{{ $errors->first('asunto') }}</small>
                      @endif
                  </div>
              <textarea class="form-control{{ $errors->has('mensaje') ? ' is-invalid' : '' }}" id="mensaje" name="mensaje" placeholder="Escribe tu mensaje aquí..." value="{{ old('mensaje') }}" rows="5"></textarea>
              @if($errors->has('mensaje'))
                          <small class="form-text invalid-feedback">{{ $errors->first('mensaje') }}</small>
                      @endif
              <div class="row my-4">
                  <div class="col-lg-6 offset-lg-3">
                    <button class="btn btn-outline-secondary btn-master text-center btn-lg" type="submit">Enviar</button>
                  </div>
              </div>            
          </form>
      </div>
    </div>
  </div>


  <script>
    $(document).ready(function(){

      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
          // Prevent default anchor click behavior
         //event.preventDefault();

          // Store hash
          var hash = this.hash;
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 900, function(){     
              // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
          });
      });
      
      // Slide in elements on scroll
      $(window).scroll(function() {
        $(".slideanim").each(function(){
          var pos = $(this).offset().top;

          var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
              $(this).addClass("slide");
            }
        });
      });
    });
  </script>
@stop

