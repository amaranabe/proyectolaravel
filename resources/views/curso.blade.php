@extends('layouts.master')
@section('description', 'Información descriptiva del contenido del curso')
@section('title'){!! $curso->titulo !!} @stop
@section('jumbotron')
    @include('partials.cursos.header_curso')
@stop

@section('contenido')
<div class="container wrapper-border">
	<!-- Section acerca de -->
	<div id="acercade" class="row">
		<div class="col-md-10 offset-md-1">
			<div class="row pt-3 border-bottom text-center wr-detalle-curso">
				<div class="col-md-4">
					<p><i class="fa fa-clock-o"></i> 20 horas</p>
				</div>
				<div class="col-md-4">
					<p><i class="fa fa-paperclip"></i> 5 recursos</p>
				</div>
				<div class="col-md-4">
					<p><i class="fa fa-graduation-cap"></i> Certificado</p>
				</div>
			</div>

			<div class="text-muted border-bottom mt-5">
				<h3>Acerca de</h3>
			</div>

			<div class="mt-3 text-secondary">
				<h5 class="text-muted">{{ __('Dirigido a') }}</h5>
				<div class="alert">
					<p>{{ $curso -> dirigido_a }}</p>
				</div>
			</div>
			<div class="mt-3 text-secondary">
				<h5 class="text-muted">{{ __('Objetivo') }}</h5>
				<ul class="alert">
					@forelse($curso->objetivos as $objetivo)
						<li class="">{{ $objetivo -> objetivo }}</li>
					@empty
						<div><i class="fa fa-info-circle"></i>&nbsp;{{ __("No hay objetivos para este curso.") }} 
						</div>
					@endforelse
				</ul>
			</div>
		</div>
	</div>
	<!-- Section Valoración -->
	@include('partials.cursos.valoraciones')
</div>
@stop

