<div class="row">
	<div class="col-md-12">
		<!-- Cabecera del curso -->
		<div class="jumbotron pt-5 pb-1">
		  	<div class="container">
		    	<div class="row">
		    		<div class="col-md-8">
			    		<h1>{{ $curso->titulo }}</h1>
			    		<h4><span class="badge badge-secondary">{{ $curso->categoria->nombre }}</span></h4>
			    		<h5>{{ __("Actualizado") }}: {{ $curso->created_at->format('d/m/Y') }}</h5>
			    		@include('partials.cursos.rating')
			    	</div>	
		    	</div>
		  	</div>
		</div>
	</div>
</div>	