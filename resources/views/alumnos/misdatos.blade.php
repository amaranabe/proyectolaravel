@extends('layouts.master')
@section('description', 'Acceso a los datos personales y su posible configuración')
@section('title', 'Perfíl')
@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __(" Mis datos"),
        "icono" => "user-circle-o",
        "query" => "Accede a la información de tu perfil"
    ])
@stop

@section('contenido')
<section class="container wrapper-border">
    <div class="my-4">
        <div class="row justify-content-center">
            <div class="col-sm-12 text-center">
                <div class="container">
                    <!-- message from controller-->
                    @if(session('profile_message'))
                            <div class="alert alert-success text-center mb-5" role="alert">
                                {{ session('profile_message') }}
                            </div>
                    @endif
                    <!-- form user info -->
                    <div class="card">
                        <div class="card-header text-center">
                            <h5>Información de tu perfil</h5>
                        </div>
                        <div class="card-body text-sm-left">
                            <div class="row">
                                <!-- Menú Perfil -->
                                <div class="col-sm-3 border-right">
                                    <div class="nav flex-column nav-pills" id="perfil-menu" role="tablist" aria-orientation="vertical">
                                      <a class="nav-link{{ count($errors->password_div)==0 ? ' active' : '' }}" id="misdatos-menu" data-toggle="pill" href="#misdatos" role="tab" aria-controls="misdatos" aria-selected="true">Mis datos</a>
                                      <a class="nav-link{{ count($errors->password_div)>0 ? ' active' : '' }}" id="cambiar-menu" data-toggle="pill" href="#cambiarpassword" role="tab" aria-controls="cambiarpassword" aria-selected="false">Cambiar contraseña</a>
                                      <a class="nav-link" id="baja-menu" data-toggle="pill" href="#baja" role="tab" aria-controls="baja" aria-selected="false">Darme de baja</a>
                                    </div>
                                </div>
                                <!-- Contenido Perfil -->
                                <div class="col-sm-9">
                                    <div class="tab-content m-5" id="perfil-contenido">
                                    
                                    <!-- -- Contenido: Mis datos -->
                                    <div class="tab-pane fade{{ count($errors->password_div)==0 ? ' show active' : '' }}" id="misdatos" role="tabpanel" aria-labelledby="misdatos-menu">

                                        <form method="POST" action="{{ url('/user/editar-perfil')}}" role="form" autocomplete="off">
                                            {{ csrf_field() }}

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label form-control-label">{{ __('Nombre') }}</label>
                                                <div class="col-md-6">
                                                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" name="name" value="{{ old('name') ?: $user->name }}">
                                                    @if($errors->has('name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label form-control-label">{{ __('Primer Apellido') }}</label>
                                                <div class="col-md-6">
                                                    <input class="form-control{{ $errors->has('apellido1') ? ' is-invalid' : '' }}" type="text" name="apellido1" value="{{ old('apellido1') ?: $user->apellido1 }}">
                                                    @if ($errors->has('apellido1'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('apellido1') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>          
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label form-control-label">{{ __('Segundo Apellido') }}</label>
                                                <div class="col-md-6">
                                                    <input class="form-control{{ $errors->has('apellido2') ? ' is-invalid' : '' }}" type="text" name="apellido2" value="{{ old('apellido2') ?: $user->apellido2 }}">
                                                    @if ($errors->has('apellido2'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('apellido2') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label form-control-label">{{ __('Email') }}</label>
                                                <div class="col-md-6">
                                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" value="{{ old('email') ?: $user->email }}">
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group mt-4">
                                                <div class="col-md-6 offset-4 col-12 d-inline-flex justify-content-center">
                                                    <button type="submit" class="btn btn-outline-secondary btn-master btn-lg">
                                                    {{ __('Guardar') }}
                                                    </button>
                                                    <a href="{{ url('/home') }}" class="btn btn-outline-secondary btn-lg mx-4">Cancelar</a>
                                                </div>
                                            </div>

                                        </form>
                                    </div>


                                      <!-- -- Contenido: Cambiar Contraseña -->
                                    <div class="tab-pane fade{{ count($errors->password_div)>0 ? ' show active' : '' }}" id="cambiarpassword" role="tabpanel" aria-labelledby="cambiar-menu">
                                          <div>
                                              @if (session('password_error'))
                                                <div class="alert alert-danger">
                                                    {{ session('password_error') }}
                                                </div>
                                                @endif
                                                <form method="POST" action="{{ url('/user/editar-cuenta')}}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <label for="password" class="col-md-4 col-form-label">{{ __('Contraseña actual') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control{{ $errors->password_div->has('password') ? ' is-invalid' : '' }}" name="password">

                                                            @if($errors->password_div->has('password'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->password_div->first('password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="newpassword" class="col-md-4 col-form-label">{{ __('Nueva Contraseña') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="newpassword" type="password" class="form-control{{ $errors->password_div->has('newpassword') ? ' is-invalid' : '' }}" name="newpassword">

                                                            @if($errors->password_div->has('newpassword'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->password_div->first('newpassword') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Confirmar Contraseña') }}</label>
                                                        <div class="col-md-6">
                                                            <input id="newpassword-confirm" type="password" class="form-control{{ $errors->password_div->has('newpassword_confirmation') ? ' is-invalid' : '' }}" name="newpassword_confirmation">
                                                            @if($errors->password_div->has('newpassword_confirmation'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->password_div->first('newpassword_confirmation') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group mt-4">
                                                        <div class="col-md-6 offset-4 d-inline-flex justify-content-center">
                                                            <button type="submit" class="btn btn btn-outline-secondary btn-master btn-lg">
                                                            {{ __('Guardar') }}
                                                            </button>
                                                            <a href="{{ url('/home') }}" class="btn btn-outline-secondary btn-lg mx-3">Cancelar</a>
                                                        </div>
                                                    </div>
                                                </form>
                                          </div>
                                      </div>
                                      <!-- -- Contenido: Darme de Baja -->
                                      <div class="tab-pane fade" id="baja" role="tabpanel" aria-labelledby="baja-menu">
                                            <form method="POST" action="{{ url('/user/baja') }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <div id="eliminar-cuenta">
                                                    <p class="text-center">
                                                        <b class="text-danger-dark">Advertencia:</b><br>
                                                        Si cierras tu cuenta, se cancelará tu suscripción a tus 5 cursos y perderá el acceso para siempre.
                                                    </p>
                                                    <form method="post" action="">
                                                        <div class="form-group d-flex justify-content-center">
                                                            <input type="hidden" name="user" value="{{ $user->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg mx-3">Eliminar cuenta</button><a href="{{ url('/home') }}" class="btn btn-outline-secondary btn-lg mx-3">Cancelar</a>       
                                                        </div>
                                                    </form>
                                                </div>
                                            </form>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /form user info -->
                </div>
            </div>
        </div>
    </div>
</section>
@stop

