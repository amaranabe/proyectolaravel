@extends('layouts.master')

@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __(" Mis cursos"),
        "icono" => "address-book",
        "query" => "Accede a tus cursos de un forma rápida"
    ])
@stop

@section('contenido')
<section class="container wrapper-border">

    <div class="row">
        <!-- Mensaje de aviso -->
        <div class="col-md-12 mb-3">
            @if(session('mensaje'))
                <div class="">
                    <div class="alert alert-success text-center">
                        {{ session('mensaje') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
        </div>
        @forelse($miscursos as $curso)
            <div class="col-md-4">
                @include('partials.card')
            </div>
        @empty
            <div class="col-sm-12"> 
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title">{{ __("No hay ningún curso disponible.") }}</h5>
                    </div>
                </div>
            </div>  
        @endforelse
    </div>

</section>
@stop
