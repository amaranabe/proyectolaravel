@extends('layouts.master')
@section('description', 'Acceso online a elearning')
@section('title'){!! $curso->titulo !!} @stop
@section('jumbotron')
    @include('alumnos.partials.header_elearning')
@stop

@section('contenido')
	<div class="container wrapper-border">
		<div class="row mb-5">
			<div class="col-md-4" style="font-size:15px;">
				<!-- Incluimos la lista de temas con sus correspondientes recursos -->
				@include('partials.cursos.programa')
			</div>
			<div class="col-md-8">
				<div id="recurso" class="row wr_recurso_file">
					<div class="col-lg-12 my-3">
						<div class="border-bottom mb-3">
							<h3 class="text-muted">Recursos</h3>
						</div>
						
							@php 
								$id = $curso ->temas[0]->recursos[0]->id;
								$ext = $curso ->temas[0]->recursos[0]->extension;
								$url = url('curso/learning/show').'/'. $id;
							@endphp
							@if($curso ->temas[0]->recursos[0]->tipo == "documento")
								<div id="fileWrapper" class="embed-responsive embed-responsive-1by1">
									<object class="embed-responsive-item" data="{{ $url }}" type="application/{{ $ext }}">
					                	<embed src="{{ $url }}" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
					                    alt :<a href="{{ $url }}">
					                </object>
								</div>
							@else
								<h5 class="text-center">{{ __('Debes elegir un recurso del tema.')}}</h5>
							@endif
						
					</div>
				</div>
			</div>
		</div>

		<!-- Section Valoración -->
		@include('partials.cursos.valoracion_form')
		@include('partials.cursos.valoraciones')
	</div>
@stop


@push('scripts')

<script>

	/**
	 * Display a html tag to show a differten type of file
	 * @return string
	 */
	function showFile(id, cursoID, tipo, ext, mimetype) {

		var url= "{{ url('curso/learning/show') }}/" + id;
		$('#collapseAudio').remove('#collapseAudio');
      	if(tipo == "audio") {
      	 		$('.show-file').each(function(){
      	 			if($(this).data('id') == id ) {
      	 				$(this).after('<div id="collapseAudio"></div>');
		      	 		var audio = '<audio id="playerAudio" class="embed-responsive-item" preload="auto" controls>' 
		      	 			+ '<source src="' + url + '" type="audio/' + ext +'">' 
		      	 			+ '<p>Your browser doesn\'t support HTML5 audio. Here is a <a href="'+ url +'">link to the audio</a> instead.</p>' 
		      	 			+ '</audio>';
			     		$('#collapseAudio').html(audio);

      	 			}     	 			
      	 		});
	    }
	    else if (tipo == "video") {
	    	 var video = '<video src="'+ url +'" controls type="' + mimetype +'">Tu navegador no implementa el elemento <code>video</code>.</video>';
	    	$('#fileWrapper').html(video);
	    }
		else  {	
			var documento ='<object class="embed-responsive-item" data="'+ url + '" type="application/'+ ext + '"><embed src="'+ url +'" width="600" height="500" alt="'+ ext +'" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">alt :<a href="'+ url +'">            </object>';
			$('#fileWrapper').html(documento);
		}
	}

    $(document).ready(function(){
        // accordion pluse minus goes here        
       $('.accordion-toggle').click(function(){
          var has = $(this);
          console.log(has);
          if(has.hasClass('collapsed')){
             jQuery(this).find('i').removeClass('fa fa-plus-circle');
             jQuery(this).find('i').addClass('fa fa-minus-circle');
          }
          else{
            jQuery(this).find('i').removeClass('fa fa-minus-circle');
            jQuery(this).find('i').addClass('fa fa-plus-circle');
          }
        });

    });

  </script>
@endpush