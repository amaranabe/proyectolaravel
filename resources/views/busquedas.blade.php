@extends('layouts.master')
@section('description', 'Li')
@section('title', 'Cesta de la compra de cursos que ofertan recursos para apoyo a personas con discapacidad intelectual')
@section('jumbotron')
    @include('partials.jumbotron', [
        "titulo" => __("Resultados de búsqueda"),
        "icono" => "search",
        "query" => ""
    ])
    
@stop

@section('contenido')
<section class="container wrapper-border">
    <div class="">
        <h5>{{ count($cursos) }} resultados para <strong>{{ $query }}</strong></h5>
    </div>
    <div class="px-5 my-5">
        <div class="row justify-content-center">
            @forelse($cursos as $curso)
                @include('partials.card')
            @empty
                <div class="alert alert-danger">
                    {{ __("No hay ningún curso disponible") }}
                </div>
            @endforelse
        </div>
        <div class="row justify-content-center mt-4">
            {{ $cursos -> links () }}
        </div>
    </div>
</section>
@stop
